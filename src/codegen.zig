const std = @import("std");
const StructBuilder = @import("StructBuilder.zig");
const ImportConsts = @import("ImportConsts.zig");
// ISSUE:
// Code shortcut that are incorrect, hardcoded, or just feel bad should be commanded with `//Hacky`

/// string to camel case.
/// Specificlly,
/// WORD_OTHERWORD -> WordOtherword
/// WORD[32][D] ->  Word2D/Word3D
fn toCamelCase(buf: []u8, src: []const u8, to_lower: bool) []const u8 {
    var out: u64 = 0;
    var cap = true;
    var last: u8 = 0;
    for (src) |c| {
        if (c == '_') {
            cap = true;
            continue;
        }
        defer last = c;
        defer out += 1;
        cap = cap or ((last == '3' or last == '2') and c == 'D');
        if (cap) {
            buf[out] = std.ascii.toUpper(c);
            cap = false;
        } else if (to_lower) {
            buf[out] = std.ascii.toLower(c);
        } else {
            buf[out] = c;
        }
    }
    return buf[0..out];
}

/// ONLY VALID AFTER gen_type_impl
pub var CannonVariantTypes: ?[][]const u8 = undefined;

// List of Objects{"build_configuration"}
const Parsed = std.json.Parsed(std.json.Value);

/// Writes all args from `_args` wrapped in a &[_]*anyopaque{}.
/// Each arg is also @ptrCast
/// EG: `&[_]*anyapque{@ptrCast(arg.ptr), ...}`
fn append_args_in_anyopaque(_args: ?std.json.Value, writer: anytype, append_idx: bool, is_vararg: bool) !u64 {
    var max: u64 = 0;
    // Shortcircuit for 1 argument varargs
    if (_args) |args| if (args.array.items.len == 1 and is_vararg) {
        if (append_idx) {
            try writer.print("@ptrCast(", .{});
            try StructBuilder.write_safe_identifier_append_idx(args.array.items[0].object.get("name").?.string, writer, 0);
            try writer.print(".ptr)", .{});
            // try writer.print("@ptrCast({s}{d}.ptr)", .{ args.array.items[0].object.get("name").?.string, 0 });
        } else {
            try writer.print("@ptrCast(", .{});
            try StructBuilder.write_safe_identifier(args.array.items[0].object.get("name").?.string, writer);
            try writer.print(".ptr)", .{});
            // try writer.print("@ptrCast({s}.ptr)", .{args.array.items[0].object.get("name").?.string});
        }
        return 0;
    };

    _ = try writer.write("&[_]*const anyopaque{");
    if (_args) |args| {
        if (is_vararg) {
            // This vararg function will end up with:
            // &[_]*const anyopaque{arg1,arg2...,vararg.ptr.*} which is wrong
            // I don't know the correct way to generate an anyopauqe array for
            // this without using zig vardiac or heap allocation. We can't
            // know in comptime the size of the argument array passed to this function
            std.log.err("Improperly handled vararg function. Please fix me!", .{});
        }
        for (args.array.items, 0..) |arg, i| {
            const should_vararg = is_vararg and args.array.items.len - 1 == i;
            if (append_idx) {
                try writer.print("{s}{s}{d}{s}, ", .{
                    if (should_vararg) "" else "&",
                    arg.object.get("name").?.string,
                    i,
                    if (should_vararg) ".ptr.*" else "",
                });
            } else {
                try writer.print("{s}{s}{s}, ", .{
                    if (should_vararg) "" else "&",
                    arg.object.get("name").?.string,
                    if (should_vararg) ".ptr.*" else "",
                });
            }
            max = i;
        }
    }
    _ = try writer.write("}");
    return max;
}
/// Moves Variant.Type.* from global_enums into types.Variant
fn type_impl_addition_variant_attributes(parsed: *Parsed, builder: *StructBuilder) !void {
    // Move enums into Variant
    const buff = try std.heap.page_allocator.alloc(u8, std.mem.page_size);
    var fb = std.heap.FixedBufferAllocator.init(buff);
    var alloc = fb.allocator();
    // Because this is the first item allocated I should be able to use free it to free the whole page.
    // 40 * 8 = 320bytes
    // 4kb - 320 = 3776b. should fit
    var types = try std.ArrayList([]const u8).initCapacity(alloc, 40);
    defer CannonVariantTypes = types.toOwnedSlice() catch unreachable;
    for (parsed.value.object.get("global_enums").?.array.items) |e| {
        const enum_name = e.object.get("name").?.string;
        _ = std.mem.indexOf(u8, enum_name, "Variant.") orelse continue;
        try builder.start_struct(true, .Const, .Enum, enum_name["Variant.".len..], .{});
        for (e.object.get("values").?.array.items) |value| {
            var buffer: [64]u8 = undefined;
            const _name = value.object.get("name").?.string;
            var name = _name;
            const val = value.object.get("value").?.integer;
            if (std.mem.eql(u8, enum_name, "Variant.Type")) {
                // Force upper case for RID and AABB
                const to_lower = if (std.mem.indexOf(u8, name, "RID")) |_| false else if (std.mem.indexOf(u8, name, "AABB")) |_| false else true;
                name = toCamelCase(&buffer, name[4..], to_lower);
                if (!std.ascii.eqlIgnoreCase("max", name)) try types.append(try alloc.dupe(u8, name));
            }

            try builder.add_enum_field(name, val, .{});
        }
        try builder.end_struct();
    }
}

/// Adds strictly non-api derived atrributes that I believe should be easliy accessible on the object
/// Called at tail/(end before end_struct)
fn type_impl_class_tail_hook(parsed: *Parsed, builder: *StructBuilder, class: []const u8) !void {
    if (std.mem.eql(u8, class, "Variant")) {
        try type_impl_addition_variant_attributes(parsed, builder);
        var qsf = QuickStringFmt(64, "to_{s}"){};
        if (CannonVariantTypes) |cvt| for (cvt) |type_name| {
            try builder.start_fn(true, try qsf.get(.{type_name}), .{ .arg_type = "Variant", .is_ptr = true }, .{});
            try builder.end_fn_args(type_name, .{});
            try builder.add_global_field(false, .Var, "target_type", type_name, "undefined", .{});
            try builder.print_with_indent("API_REF.VariantApi.to_{s}.?(@ptrCast(&target_type), @ptrCast(self));\n", .{type_name});
            try builder.print_with_indent("return target_type;\n", .{});
            try builder.end_fn();
        };
        // try builder.start_fn(true, "get_variant_type", .{ .arg_type = "Variant", .is_ptr = true, .is_const = true }, .{ .use_safe_type = false });
        // try builder.end_fn_args("Variant.Type", .{});
        // try builder.print_with_indent(
        //     "return @enumFromInt(Variant.Type, fns.variant_get_type(self));\n",
        // );
    } else {
        try builder.start_fn(true, "to_variant", .{ .arg_type = class, .is_ptr = true, .is_const = true }, .{});
        try builder.end_fn_args("?Variant", .{ .use_safe_type = false });
        try builder.print_with_indent(
            \\if (API_REF.{s}Api.to_variant) |tovariant| {{
            \\            var variant: {s}.Variant = undefined;
            \\            tovariant(@ptrCast(&variant), @ptrCast(@constCast(self)));
            \\            return variant;
            \\        }}
            \\        return null;
            \\
        , .{ class, ImportConsts.TYPE });
        // try builder.print_with_indent(
        //     "if (API_REF.{s}Api.to_variant) |tovariant| {{ var variant: " ++ ImportConsts.TYPE ++ ".Variant = undefined; tovariant(@ptrCast(&variant), @ptrCast(@constCast(self))); return variant;}} return null;",
        //     .{class},
        // );
        try builder.end_fn();
    }
}
/// Adds strictly non=api derived attributes
/// Called at head/(immediately after start_struct)
fn type_impl_head_hook(builder: *StructBuilder, class: []const u8) !void {
    if (std.mem.eql(u8, class, "Variant")) {} else {
        var qsf = QuickStringFmt(64, ".{s}"){};
        var tag = try qsf.get_mut(.{class});
        // .bool => .Bool
        // .float => .Float
        tag[1] = std.ascii.toUpper(tag[1]);
        try builder.add_global_field(true, .Const, "GD_TYPE", "Variant.Type", tag, .{ .use_safe_type = false });
    }
}

/// Generates constructors(init[0-9]+)
fn type_impl_ctor(parsed: *Parsed, builder: *StructBuilder, class: []const u8, ctor: *std.json.Value) !void {
    // TODO make this return the type instead of just initlizing a pointer
    var buff: [20]u8 = undefined;
    const ctorname = try std.fmt.bufPrint(&buff, "init{d}", .{ctor.object.get("index").?.integer});
    const init_returns_object = true;
    if (init_returns_object) {
        try builder.start_fn(true, ctorname, null, .{ .use_safe_type = false });
    } else {
        try builder.start_fn(true, ctorname, .{ .is_ptr = true, .arg_type = class }, .{ .use_safe_type = false });
    }
    if (ctor.object.get("arguments")) |args| for (args.array.items, 0..) |arg, i| {
        try builder.add_fn_arg(
            arg.object.get("name").?.string,
            .{ .arg_type = arg.object.get("type").?.string, .append_number = i },
            .{ .use_safe_type = false, .use_safe_identifier = false },
            i == args.array.items.len - 1,
        );
    };
    if (init_returns_object) {
        try builder.end_fn_args("@This()", .{ .use_safe_type = false });
        try builder.write_with_indent("var self: @This() = undefined;\n");
    } else {
        try builder.end_fn_args("void", .{ .use_safe_type = false });
    }
    // API_REF.StringApi.initx.?(self, &[_]*const anyopaque{...
    try builder.print_with_indent("API_REF.{s}Api.", .{class});
    try StructBuilder.write_safe_identifier(ctorname, builder.writer);
    if (init_returns_object) {
        _ = try builder.writer.write(".?(&self, &[_]*const anyopaque{");
    } else {
        _ = try builder.writer.write(".?(self, &[_]*const anyopaque{");
    }
    if (ctor.object.get("arguments")) |args| {
        for (args.array.items, 0..) |arg, i| {
            try builder.writer.print("&{s}{d}, ", .{ arg.object.get("name").?.string, i });
        }
    }
    _ = try builder.writer.write("});\n");
    if (init_returns_object) {
        try builder.write_with_indent("return self;\n");
    }

    try builder.end_fn();
    _ = parsed; // autofix
}

fn gen_type_impl(parsed: *Parsed, build_config: BuildConfig) !void {
    const builtin_class_sizes = parsed.value.object.get("builtin_class_sizes").?;
    const builtin_class_offsets = parsed.value.object.get("builtin_class_member_offsets").?;
    const Pair = struct {
        list: std.ArrayList(u8),
        builder: StructBuilder,
    };
    // Hacky
    var map = std.StringHashMap(Pair).init(std.heap.page_allocator);
    defer map.deinit();
    var classes: ?std.json.Value = null;
    for (builtin_class_offsets.array.items) |buildconf| {
        if (std.meta.stringToEnum(BuildConfig, buildconf.object.get("build_configuration").?.string) == build_config) {
            classes = buildconf.object.get("classes");
            break;
        }
    }
    if (classes == null) @panic("no matching build_config\n");
    for (classes.?.array.items) |class| {
        const class_name = class.object.get("name").?.string;
        try map.put(class_name, .{ .list = std.ArrayList(u8).init(std.heap.page_allocator), .builder = undefined });
        var pair = map.getPtr(class_name).?;
        pair.builder = StructBuilder{ .writer = pair.list.writer().any() };
        try pair.builder.start_struct(true, .Const, .Struct, class_name, .{});
        try type_impl_head_hook(&pair.builder, class_name);

        for (class.object.get("members").?.array.items) |member| {
            try pair.builder.add_instance_field(member.object.get("member").?.string, StructBuilder.get_member_type(member), null, .{});
        }
    }
    classes = null;
    for (builtin_class_sizes.array.items) |buildconf| {
        if (std.meta.stringToEnum(BuildConfig, buildconf.object.get("build_configuration").?.string) == build_config) {
            classes = buildconf.object.get("sizes");
            break;
        }
    }
    if (classes == null) @panic("no matching build_config2\n");
    for (classes.?.array.items) |class| {
        // "sizes": [
        // 	{
        // 		"name": "Nil",
        // 		"size": 0
        // 	},
        // 	{
        // 		"name": "bool",
        // 		"size": 1
        // 	},
        const key = class.object.get("name").?.string;
        var pair: *Pair = undefined;
        if (map.getPtr(class.object.get("name").?.string)) |list| {
            pair = list;
        } else {
            try map.put(key, .{ .list = std.ArrayList(u8).init(std.heap.page_allocator), .builder = undefined });
            pair = map.getPtr(key).?;
            pair.builder = StructBuilder{ .writer = pair.list.writer().any() };
            try pair.builder.start_struct(true, .Const, .Struct, class.object.get("name").?.string, .{});
            try type_impl_head_hook(&pair.builder, class.object.get("name").?.string);
            // Can not generate the sized buffed.
            var qsf = QuickStringFmt(32, "[{d}]*anyopaque"){};
            const field_type = switch (class.object.get("size").?.integer) {
                0 => "void",
                1 => "u8",
                else => |i| try qsf.get(.{@divExact(i, 8)}),
            };
            // try pair.builder.print_with_indent("no_touch: [{d}]u8,\n", .{class.object.get("size").?.integer});
            try pair.builder.print_with_indent("no_touch: {s},\n", .{field_type});
        }
        try pair.list.writer().print(
            \\    test {{
            \\        @import("std").testing.expect(@sizeOf(@This()) == {d});
            \\    }}
            \\
        , .{class.object.get("size").?.integer});
    }
    const builtin_classes = parsed.value.object.get("builtin_classes").?;
    for (builtin_classes.array.items) |class| {
        // Hacky
        const class_name = class.object.get("name").?.string;
        var pair = map.getPtr(class_name).?;
        pair.builder.writer = pair.list.writer().any();
        try pair.builder.print_with_indent("// is_keyed: {any}\n", .{class.object.get("is_keyed").?.bool});
        if (class.object.get("operators")) |operators| {
            _ = operators;
            try pair.builder.print_with_indent("// ~~~~~~~ Operators ~~~~~~~\n", .{});
        }
        if (class.object.get("methods")) |methods| {
            try pair.builder.print_with_indent("// ~~~~~~~ Methods ~~~~~~~\n", .{});
            for (methods.array.items) |method| {
                const method_name = method.object.get("name").?.string;
                const is_static = method.object.get("is_static").?.bool;
                const is_vararg = method.object.get("is_vararg").?.bool;
                const is_const = method.object.get("is_const").?.bool;
                try pair.builder.print_with_indent("// static: {any} | vararg: {any} | const: {any}\n", .{
                    is_static,
                    is_vararg,
                    is_const,
                });
                try pair.builder.start_fn(true, method_name, if (is_static)
                    null
                else
                    .{ .is_ptr = true, .arg_type = class_name, .is_const = is_const }, .{ .use_safe_type = false });
                defer pair.builder.end_fn() catch unreachable;

                const has_rt = method.object.get("return_type") != null;
                if (method.object.get("arguments")) |args| for (args.array.items, 0..) |arg, i| {
                    try pair.builder.add_fn_arg(
                        arg.object.get("name").?.string,
                        .{ .arg_type = arg.object.get("type").?.string, .append_number = i },
                        .{ .use_safe_type = false, .use_safe_identifier = false },
                        i == args.array.items.len - 1,
                    );
                };

                try pair.builder.end_fn_args(if (has_rt) method.object.get("return_type").?.string else "void", .{ .use_safe_type = false });
                // EG:
                // pub fn @"asdf 1234"([self: *type, ][arg0: Arg0T...]) [RT|void] {
                //    [var rt: RT = undefined;]
                //    API_REF.GDTYPEApi.asdf(self, &[_]*const anyopaque{[&arg0...]}, [rt|null], [max(x) for argx]);
                //    [return rt];
                //}
                if (has_rt) try pair.builder.add_global_field(false, .Var, "rt", method.object.get("return_type").?.string, "undefined", .{});
                try pair.builder.print_with_indent("API_REF.{s}Api.", .{
                    class_name,
                });
                var buff2: [128]u8 = undefined;
                const API_method_name = try std.fmt.bufPrint(&buff2, "@\"{s} {d}\"", .{ method_name, method.object.get("hash").?.integer });
                try StructBuilder.write_safe_identifier(API_method_name, pair.builder.writer);
                _ = try pair.builder.writer.write(".?(");
                if (!is_static) {
                    _ = try pair.builder.writer.write("@constCast(self), ");
                }

                const max = try append_args_in_anyopaque(method.object.get("arguments"), pair.builder.writer, true, false);
                try pair.builder.writer.print(", {s}, {d});\n", .{ if (has_rt) "&rt" else "null", max + 1 });
                if (has_rt) try pair.builder.write_with_indent("return rt;\n");
            }
        }
        for (class.object.get("constructors").?.array.items) |*ctor| {
            try type_impl_ctor(parsed, &pair.builder, class_name, ctor);
        }
        if (!class.object.get("has_destructor").?.bool) {
            try pair.builder.write_with_indent("/// This destructor will not exist.\n");
        }
        try pair.builder.start_fn(true, "deinit", .{ .arg_type = class_name, .is_ptr = true, .is_const = true }, .{});
        try pair.builder.end_fn_args("void", .{});
        try pair.builder.print_with_indent("if (API_REF.{s}Api.deinit) |nn_dctor| nn_dctor(@constCast(self));\n", .{class_name});
        try pair.builder.end_fn();
    }
    var file = try std.fs.cwd().createFile("gdextension/gdextension_type_impl.zig", .{});
    defer file.close();
    try write_imports(file.writer(), .{
        .gd_const = true,
    });
    _ = try file.writer().write("pub const " ++ ImportConsts.TYPE ++ " = @This();\n");
    _ = try file.writer().write("pub const API_REF = @import(\"gdextension_type_api.zig\");\n");
    // var iter = map.valueIterator();
    // _ = iter; // autofix
    var iterk = map.keyIterator();
    while (iterk.next()) |key| {
        var pair = map.getPtr(key.*).?;
        // Why is this needed??
        pair.builder.writer = pair.list.writer().any();
        try type_impl_class_tail_hook(parsed, &pair.builder, key.*);
        try pair.builder.end_struct();
        try file.writeAll(pair.list.items);
        pair.list.deinit();
    }
}

fn write_header_warning(writer: anytype) !void {
    _ = try writer.write(
        \\   // zig fmt: off
        \\  ////////////////////////////
        \\ // THIS FILE IS GENERATED //
        \\////////////////////////////
        \\
    );
}
/// Writes FILE GENERATED HEADER and imports needed for the generated file
fn write_imports(
    writer: anytype,
    opts: struct {
        gd_const: bool = false,
        types: bool = false,
        type_api: bool = false,
        enums: bool = false,
        utils: bool = false,
        classes: bool = false,
        classes_api: bool = false,
        native_struct: bool = false,
        singletons: bool = false,
        interface: bool = false,
        api: bool = false,
    },
) !void {
    try write_header_warning(writer);
    if (opts.gd_const) _ = try writer.write("const " ++ ImportConsts.CONST ++ " = @import(\"gdextension_global_constants.zig\");\n");
    if (opts.types) _ = try writer.write("const " ++ ImportConsts.TYPE ++ " = @import(\"gdextension_type_impl.zig\");\n");
    if (opts.type_api) _ = try writer.write("const " ++ ImportConsts.TYPE_API ++ " = @import(\"gdextension_type_api.zig\");\n");
    if (opts.enums) _ = try writer.write("const " ++ ImportConsts.ENUMS ++ " = @import(\"gdextension_global_enums.zig\");\n");
    if (opts.utils) _ = try writer.write("const " ++ ImportConsts.UTIL_FUNC ++ " = @import(\"gdextension_utility_functions.zig\");\n");
    if (opts.classes) _ = try writer.write("const " ++ ImportConsts.CLASSES ++ " = @import(\"gdextension_classes_impl.zig\");\n");
    if (opts.classes_api) _ = try writer.write("const " ++ ImportConsts.CLASSES_API ++ " = @import(\"gdextension_classes_api.zig\");\n");
    if (opts.native_struct) _ = try writer.write("const " ++ ImportConsts.NATIVE ++ " = @import(\"gdextension_native_structures.zig\");\n");
    if (opts.singletons) _ = try writer.write("const " ++ ImportConsts.SINGLETON ++ " = @import(\"gdextension_singletons.zig\");\n");
    if (opts.interface) _ = try writer.write("const " ++ ImportConsts.INTERFACE ++ " = @import(\"gdextension_interface.zig\");\n");
    if (opts.api) _ = try writer.write("const " ++ ImportConsts.API ++ " = @import(\"gdapi\");\n");
    _ = try writer.write("\n");
}

/// Determines which config mode to use from api_extension.json
/// I don't know the difference between float_** and double_**
pub const BuildConfig = enum {
    float_32,
    float_64,
    double_32,
    double_64,
};

/// Generated helper file for use in other generated files
/// Includes:
/// * the GodotVersion
/// * the BuildConfig
/// * type_assumed() to indicate a type used the default fallthrough for _write_type
/// * typed_array()
fn gen_constants(parsed: *const std.json.Value, build_config: BuildConfig) !void {
    var list = std.ArrayList(u8).init(std.heap.page_allocator);
    defer list.deinit();
    var writer = list.writer();
    try write_imports(writer, .{ .types = true });
    // type_assumed is to assert that a type fell through to the default _write_types_case
    _ = try writer.write("pub fn type_assumed(t: type) type {return t;}\n");
    _ = try writer.write("pub fn typed_array(_: type) type {return " ++ ImportConsts.TYPE ++ ".Array;}\n\n");

    _ = try writer.write(
        \\pub const BuildConfig = enum {
        \\    float_32, float_64, double_32, double_64,
        \\};
        \\pub const BUILD_CONFIG: BuildConfig = .
    );
    try writer.print("{s};\n", .{@tagName(build_config)});

    try writer.print(
        \\pub const Version = struct {{
        \\    pub const major: u8 = {d};
        \\    pub const minor: u8 = {d};
        \\    pub const patch: u8 = {d};
        \\    pub const status = "{s}";
        \\    pub const build = "{s}";
        \\    pub const full_name = "{s}";
        \\}};
        \\
    , .{
        if (parsed.object.get("version_major")) |v| v.integer else 0,
        if (parsed.object.get("version_minor")) |v| v.integer else 0,
        if (parsed.object.get("version_patch")) |v| v.integer else 0,
        if (parsed.object.get("version_status")) |v| v.string else "\"\"",
        if (parsed.object.get("version_build")) |v| v.string else "\"\"",
        if (parsed.object.get("version_full_name")) |v| v.string else "\"\"",
    });
    var file = try std.fs.cwd().createFile("gdextension/gdextension_global_constants.zig", .{});
    defer file.close();
    try file.writeAll(list.items);
}

fn enums_tail_hook(root: ?*Parsed, builder: *StructBuilder) !void {
    try builder.start_struct(true, .Const, .Enum, "Notification", .{});
    var HITS = std.ArrayList([]const u8).init(std.heap.page_allocator);
    defer HITS.deinit();
    if (root.?.value.object.get("classes")) |classes| for (classes.array.items) |class| {
        if (class.object.get("constants")) |constants| for (constants.array.items) |constant| {
            const name = constant.object.get("name").?.string;
            const KEY = "NOTIFICATION_";
            // std.log.debug("{s} {any}", .{ name, std.mem.startsWith(u8, name, KEY) });
            if (std.mem.startsWith(u8, name, KEY)) {
                var found = false;
                for (HITS.items) |k| if (std.mem.eql(u8, k, name)) {
                    found = true;
                    break;
                };
                // DRAW = 30,
                // UPDATE_SKELETON = 50,
                // ENTER_WORLD = 41,
                // EXIT_WORLD = 42,
                if (std.mem.endsWith(u8, name, "_DRAW") or std.mem.endsWith(u8, name, "_UPDATE_SKELETON") or std.mem.endsWith(u8, name, "_WORLD")) {
                    found = true; // Cheating
                }
                if (!found) {
                    try builder.add_enum_field(name[KEY.len..], constant.object.get("value").?.integer, .{});
                    try HITS.append(name);
                }
            }
        };
    };
    try builder.end_struct();
}

fn gen_enums(parsed: *const std.json.Value, list_to_flag_nested_calling: ?*StructBuilder, root: ?*Parsed) !void {
    // "global_enums": [
    // 	{
    // 		"name": "Side",
    // 		"is_bitfield": false,
    // 		"values": [
    // 			{
    // 				"name": "SIDE_LEFT",
    // 				"value": 0
    // 			},

    var list = std.ArrayList(u8).init(std.heap.page_allocator);
    var builder = if (list_to_flag_nested_calling) |l| l else @constCast(&StructBuilder{ .writer = list.writer().any() });
    defer if (list_to_flag_nested_calling) |_| {} else list.deinit();
    const writer = list.writer();
    try write_imports(writer, .{});
    for (parsed.array.items) |e| {
        const enum_name = e.object.get("name").?.string;
        // Hacky
        if (std.mem.indexOf(u8, enum_name, "Variant.")) |_| {
            std.log.err("gen_enum skipping: {s}", .{enum_name});
            continue;
        }
        if (e.object.get("is_bitfield").?.bool) {
            try builder.start_struct(true, .Const, .Struct, enum_name, .{});
            for (e.object.get("values").?.array.items) |value| {
                try builder.print_with_indent("pub const {s}: u32 = 0b{b};\n", .{ value.object.get("name").?.string, value.object.get("value").?.integer });
            }
        } else {
            try builder.start_struct(true, .Const, .Enum, enum_name, .{});

            for (e.object.get("values").?.array.items) |value| {
                try builder.add_enum_field(value.object.get("name").?.string, value.object.get("value").?.integer, .{});
            }
        }
        // if (e.object.get("is_bitfield").?.bool) try builder.write_with_indent("// bitfield\n");
        try builder.end_struct();
    }
    if (list_to_flag_nested_calling) |_| {
        return;
    }
    // {
    // 	"name": "NOTIFICATION_TRANSFORM_CHANGED",
    // 	"value": 2000
    // },
    // {
    // 	"name": "NOTIFICATION_LOCAL_TRANSFORM_CHANGED",
    // 	"value": 35
    // },
    try enums_tail_hook(root, builder);
    var file = try std.fs.cwd().createFile("gdextension/gdextension_global_enums.zig", .{});
    defer file.close();
    try file.writeAll(list.items);
}

fn classes_tail_hook_methods(class: []const u8, api_builder: *StructBuilder, impl_builder: *StructBuilder) !void {
    _ = impl_builder; // autofix
    _ = api_builder; // autofix
    // impl_builder.writer.print(
    //     \\pub fn create() *types.Object {
    //     \\    const CLASSNAME = {s};
    //     \\    var obj = @ptrCast(fns.classdb_construct_object.?(CLASSNAME));
    //     \\    fns.object_set_instance.?(@ptrCast(obj), CLASSNAME, null);
    //     \\    return obj;
    //     \\}
    // , .{});
    _ = class; // autofix
}
fn classes_head_hook_properties(class: *const std.json.Value, cname: []const u8, api_builder: *StructBuilder, impl_builder: *StructBuilder) !void {
    _ = cname; // autofix
    _ = api_builder; // autofix
    if (class.object.get("inherits")) |i| {
        _ = i; // autofix
        try impl_builder.write_with_indent("__super_class__: SuperClass,\n");
    }
}
fn classes_tail_hook_properties(class: []const u8, api_builder: *StructBuilder, impl_builder: *StructBuilder) !void {
    _ = api_builder; // autofix
    _ = impl_builder; // autofix
    _ = class; // autofix
}

fn classes_head_hook_import(api_builder: *StructBuilder, impl_builder: *StructBuilder) !void {
    try write_imports(api_builder.writer, .{ .api = true });
    try write_imports(impl_builder.writer, .{ .api = true });
    _ = try api_builder.writer.write("const gd_convert = api.convert;\nconst fns = &api.fns;\n");
    _ = try impl_builder.writer.write("const fns = &api.fns;\n");
}

fn classes_head_hook_class(class: *const std.json.Value, class_name: []const u8, api_builder: *StructBuilder, impl_builder: *StructBuilder) !void {
    // 			"methods": [
    // 				{
    // 					"name": "set_sync_to_physics",
    // 					"is_const": false,
    // 					"is_vararg": false,
    // 					"is_static": false,
    // 					"is_virtual": false,
    // 					"hash": 2586408642,
    // 					"arguments": [
    // 						{
    // 							"name": "enable",
    // 							"type": "bool"
    // 						}
    // 					]
    // 				},
    // 			],
    if (class.object.get("inherits")) |i| {
        try api_builder.add_global_field(true, .Const, "superclass", "type", i.string, .{ .use_safe_type = false });
        try impl_builder.add_global_field(true, .Const, "SuperClass", "type", i.string, .{ .use_safe_type = false });
    } else if (std.mem.eql(u8, "Object", class.object.get("name").?.string)) {
        try impl_builder.write_with_indent("object: " ++ ImportConsts.TYPE ++ ".Object = undefined,\n");
    }
    if (class.object.get("methods")) |methods| {
        try api_builder.start_fn(true, "init", null, .{});
        try api_builder.end_fn_args("void", .{});
        defer api_builder.end_fn() catch unreachable;
        try api_builder.print_with_indent("var class_name = gd_convert.to_stringname(\"{s}\");\n", .{class_name});
        try api_builder.write_with_indent("defer class_name.deinit();\n");
        try api_builder.write_with_indent("var method_name: gd_types.StringName = undefined;\n");
        var assigments: u64 = 0;
        for (methods.array.items) |method| {
            const method_name = method.object.get("name").?.string;
            if (method.object.get("hash")) |hash| {
                try api_builder.print_with_indent("method_name = gd_convert.to_stringname(\"{s}\");\n", .{method_name});
                try api_builder.write_with_indent("");
                try api_builder._write_name(method_name, .{});
                try api_builder.writer.print(" = fns.classdb_get_method_bind.?(&class_name, &method_name, {d});\n", .{hash.integer});
                try api_builder.print_with_indent("method_name.deinit();\n", .{});
                assigments += 1;
            } else {
                try api_builder.print_with_indent("// {s} missing hash...\n", .{method_name});
            }
        }
        if (assigments == 0) {
            try api_builder.write_with_indent("_ = &method_name;\n");
            // try api_builder.write_with_indent("_ = method_name;\n");
            // try api_builder.write_with_indent("_ = class_name;\n");
        }
    }
    //
}

fn gen_classes(parsed: *const std.json.Value) !void {
    // const gd = @import("gdext").interface;
    // gd.InterfaceClassdbConstructObject; //Ctor
    // gd.InterfaceClassdbGetMethodBind; // Method reference
    // gd.InterfaceObjectMethodBindCall; // Method class
    // {
    // 			"name": "AnimatableBody2D",
    // 			"is_refcounted": false,
    // 			"is_instantiable": true,
    // 			"inherits": "StaticBody2D",
    // 			"api_type": "core",
    var file = try std.fs.cwd().createFile("gdextension/gdextension_classes_api.zig", .{});
    defer file.close();
    var api_list = std.ArrayList(u8).init(std.heap.page_allocator);
    defer api_list.deinit();
    var api_builder = StructBuilder{ .writer = api_list.writer().any() };

    var file_impl = try std.fs.cwd().createFile("gdextension/gdextension_classes_impl.zig", .{});
    defer file_impl.close();
    var impl_list = std.ArrayList(u8).init(std.heap.page_allocator);
    defer impl_list.deinit();
    var impl_builder = StructBuilder{ .writer = impl_list.writer().any() };

    try write_imports(api_builder.writer, .{
        .gd_const = true,
        .types = true,
        .enums = true,
        .native_struct = true,
        .interface = true,
    });
    try write_imports(impl_builder.writer, .{
        .classes_api = true,
        .gd_const = true,
        .types = true,
        .enums = true,
        .native_struct = true,
    });
    try classes_head_hook_import(&api_builder, &impl_builder);
    _ = try api_builder.writer.write("pub const " ++ ImportConsts.CLASSES ++ " = @This();\n");
    _ = try impl_builder.writer.write("pub const " ++ ImportConsts.CLASSES ++ " = @This();\n");
    for (parsed.array.items) |class| {
        const cname = class.object.get("name").?.string;
        try api_builder.start_struct(true, .Const, .Struct, cname, .{});
        defer api_builder.end_struct() catch unreachable;

        try impl_builder.start_struct(true, .Const, .Struct, cname, .{});
        defer impl_builder.end_struct() catch unreachable;
        try classes_head_hook_class(&class, cname, &api_builder, &impl_builder);

        try api_builder.print_with_indent("// is_refcounted: {any},\n", .{class.object.get("is_refcounted").?.bool});
        try api_builder.print_with_indent("// is_instantiable: {any},\n", .{class.object.get("is_instantiable").?.bool});
        try api_builder.print_with_indent("// api_type: {s}\n", .{class.object.get("api_type").?.string});
        try api_builder.write_with_indent("//// ENUMS ////\n");

        // "enums": [
        // 	{
        // 		"name": "FontAntialiasing",
        // 		"is_bitfield": false,
        // 		"values": [
        // 			{
        // 				"name": "FONT_ANTIALIASING_NONE",
        // 				"value": 0
        // 			},
        // 			{
        // 				"name": "FONT_ANTIALIASING_GRAY",
        // 				"value": 1
        // 			},
        // 			{
        // 				"name": "FONT_ANTIALIASING_LCD",
        // 				"value": 2
        // 			}
        // 		]
        // 	},
        if (class.object.get("enums")) |enums| {
            try gen_enums(&enums, &api_builder, null);
        }
        // 			"methods": [
        // 				{
        // 					"name": "set_sync_to_physics",
        // 					"is_const": false,
        // 					"is_vararg": false,
        // 					"is_static": false,
        // 					"is_virtual": false,
        // 					"hash": 2586408642,
        // "return_value": {
        // 	"type": "enum::Error"
        // },
        // 					"arguments": [
        // 						{
        // 							"name": "enable",
        // 							"type": "bool"
        // 						}
        // 					]
        // 				},
        // 			],
        try api_builder.write_with_indent("//// METHODS ////\n\n");
        try impl_builder.write_with_indent("//// METHODS ////\n\n");
        if (class.object.get("methods")) |methods| {
            for (methods.array.items) |method| {
                const is_static = method.object.get("is_static").?.bool;
                try api_builder.print_with_indent("// const {any}|vararg {any}|static {any}|virtual: {any}\n", .{
                    method.object.get("is_const").?.bool,
                    method.object.get("is_vararg").?.bool,
                    is_static,
                    method.object.get("is_virtual").?.bool,
                });
                const mname = method.object.get("name").?.string;
                try api_builder.add_global_field(true, .Var, mname, "interface.MethodBindPtr", "null", .{ .use_safe_type = false });
                try impl_builder.start_fn(true, mname, if (!is_static) .{
                    .arg_type = cname,
                    .is_ptr = true,
                } else null, .{});
                if (method.object.get("arguments")) |arguments| {
                    try append_fn_args(&arguments, impl_builder.writer, true);
                }
                var has_rt = false;
                var return_value: ?[]const u8 = null;
                if (method.object.get("return_value")) |rt| {
                    return_value = rt.object.get("type").?.string;
                    try impl_builder.end_fn_args(return_value.?, .{});
                    has_rt = true;
                } else {
                    try impl_builder.end_fn_args("void", .{});
                }
                // Check for null at method ptr
                try impl_builder.print_with_indent("if (" ++ ImportConsts.CLASSES_API ++ ".{s}.", .{cname});
                try StructBuilder.write_safe_identifier(mname, impl_builder.writer);
                _ = try impl_builder.writer.write(" == null){\n");
                try impl_builder.write_with_indent("unreachable;\n");
                try impl_builder.print_with_indent("}}\n", .{});
                // End Check for null

                // Call mthod
                if (has_rt) try impl_builder.add_global_field(false, .Var, "rt", return_value.?, "undefined", .{});
                // try impl_builder.print_with_indent(ImportConsts.CLASSES_API ++ ".{s}.", .{
                //     cname,
                // });
                try impl_builder.print_with_indent("fns.object_method_bind_ptrcall(" ++ ImportConsts.CLASSES_API ++ ".{s}.", .{
                    cname,
                });
                try StructBuilder.write_safe_identifier(mname, impl_builder.writer);
                // _ = try impl_builder.writer.write(".?(");
                _ = try impl_builder.writer.write(", ");
                _ = try impl_builder.writer.write(if (is_static) "null, " else "@constCast(self), ");

                _ = try append_args_in_anyopaque(method.object.get("arguments"), impl_builder.writer, true, false);
                try impl_builder.writer.print(", {s}); \n", .{if (has_rt) "&rt" else "null"});
                if (has_rt) try impl_builder.write_with_indent("return rt;\n");

                // End call mthod
                try impl_builder.end_fn();
            }
        }
        try classes_tail_hook_methods(class.object.get("name").?.string, &api_builder, &impl_builder);
        try api_builder.write_with_indent("//// Properties ////\n");
        try impl_builder.write_with_indent("//// Properties ////\n");
        // "properties": [
        // 	{
        // 		"type": "String",
        // 		"name": "ok_button_text",
        // 		"setter": "set_ok_button_text",
        // 		"getter": "get_ok_button_text"
        // 	},
        try classes_head_hook_properties(&class, cname, &api_builder, &impl_builder);
        if (true)
            if (class.object.get("properties")) |props| for (props.array.items) |prop| {
                try impl_builder.add_instance_field(prop.object.get("name").?.string, prop.object.get("type").?.string, "undefined", .{});
                try api_builder.start_struct(true, .Const, .Struct, prop.object.get("name").?.string, .{});
                defer api_builder.end_struct() catch unreachable;
                if (prop.object.get("setter")) |setter| {
                    try api_builder.add_global_field(true, .Var, setter.string, "interface.PtrSetter", "null", .{ .use_safe_type = false });
                }
                try api_builder.add_global_field(true, .Var, prop.object.get("getter").?.string, "interface.PtrGetter", "null", .{ .use_safe_type = false });
            };
        try classes_tail_hook_properties(cname, &api_builder, &impl_builder);
        try api_builder.write_with_indent("//// Signals TODO ////\n");
        try impl_builder.write_with_indent("//// Signals TODO ////\n");
    }
    try file.writeAll(api_list.items);
    try file_impl.writeAll(impl_list.items);
}

/// Quickly format a string on stack memory
pub fn QuickStringFmt(comptime size: u64, comptime fmt: []const u8) type {
    return struct {
        buff: [size]u8 = undefined,
        pub fn get(self: *@This(), args: anytype) ![]const u8 {
            return std.fmt.bufPrint(&self.buff, fmt, args);
        }
        pub fn get_mut(self: *@This(), args: anytype) ![]u8 {
            return std.fmt.bufPrint(&self.buff, fmt, args);
        }
    };
}

fn gen_utility_functions(parsed: *const std.json.Value) !void {
    var list_impl = std.ArrayList(u8).init(std.heap.page_allocator);
    defer list_impl.deinit();
    var writer_impl = list_impl.writer();
    var builder_impl = StructBuilder{ .writer = writer_impl.any() };
    try write_imports(builder_impl.writer, .{
        .types = true,
        .utils = true,
        .gd_const = true,
    });
    var list = std.ArrayList(u8).init(std.heap.page_allocator);
    defer list.deinit();
    var cat: []const u8 = "";
    var builder = StructBuilder{ .writer = list.writer().any() };
    try write_imports(builder.writer, .{
        .types = true,
        .classes = true,
        .enums = true,
        .gd_const = true,
        .interface = true,
    });
    for (parsed.array.items) |func| {
        const category = func.object.get("category").?.string;
        // If Current cat isn't previous cat
        if (!std.mem.eql(u8, cat, category)) {
            if (cat.len != 0) { // prev cat wasn't empty
                try builder.end_struct();
                try builder_impl.end_struct();
            }
            cat = category;
            try builder.start_struct(true, .Const, .Struct, category, .{});
            try builder_impl.start_struct(true, .Const, .Struct, category, .{});
        }
        // {
        // 	"name": "rid_from_int64",
        // 	"return_type": "RID",
        // 	"category": "general",
        // 	"is_vararg": false,
        // 	"hash": 3426892196,
        // 	"arguments": [
        // 		{
        // 			"name": "base",
        // 			"type": "int"
        // 		}
        // 	]
        // };
        // pub const NAME: *const fn(...) RT = undefined; // TODO
        const is_vararg = func.object.get("is_vararg").?.bool;
        if (func.object.get("is_vararg").?.bool) {
            // TODO
            try builder.write_with_indent("// vararg: NOT IMPLEMENTED\n");
            try builder_impl.write_with_indent("// vararg \n");
        }
        var qsf = QuickStringFmt(128, "@\"{s} {d}\""){};
        const name = try qsf.get(.{ func.object.get("name").?.string, func.object.get("hash").?.integer });
        try builder.add_global_field(
            true,
            .Var,
            name,
            "interface.PtrUtilityFunction",
            "null",
            .{ .use_safe_type = false },
        );
        try builder_impl.start_fn(true, func.object.get("name").?.string, null, .{});
        // ISSUE:
        // gen_function_type gives the correct arguments, but the correct type is interface.PtrUtilityFunction
        // try gen_function_type(&func, builder_impl.writer, null);
        const rt = func.object.get("return_type");
        if (func.object.get("arguments")) |args| {
            for (args.array.items, 0..) |arg, i| {
                const is_last = i + 1 == args.array.items.len;
                try builder_impl.add_fn_arg(
                    arg.object.get("name").?.string,
                    .{
                        .arg_type = arg.object.get("type").?.string,
                        .append_number = i,
                        .is_slice = is_last and is_vararg,
                        .is_ptr = is_last and is_vararg,
                        .is_const = is_last and is_vararg,
                    },
                    .{ .use_safe_identifier = false },
                    is_last,
                );
            }
        }
        try builder_impl.end_fn_args(if (rt) |r| r.string else "void", .{});
        if (rt) |r|
            try builder_impl.add_global_field(false, .Var, "rt", r.string, "undefined", .{});
        try builder_impl.print_with_indent(ImportConsts.UTIL_FUNC ++ ".{s}.{s}.?({s},", .{ category, name, if (rt) |_| "&rt" else "null" });
        const max = try append_args_in_anyopaque(func.object.get("arguments"), builder_impl.writer, true, is_vararg);
        if (is_vararg) {
            try builder_impl.writer.print(", @intCast({d}+{s}{d}.len));\n", .{
                max, // Not plus one, because the vararg should count towards this value. should be +1-1
                func.object.get("arguments").?.array.items[max].object.get("name").?.string,
                max,
            });
        } else {
            try builder_impl.writer.print(", {d});\n", .{max + 1});
        }
        if (rt) |_|
            try builder_impl.write_with_indent("return rt;\n");
        try builder_impl.end_fn();
    }
    try builder.end_struct();
    try builder_impl.end_struct();
    var file = try std.fs.cwd().createFile("gdextension/gdextension_utility_functions.zig", .{});
    defer file.close();
    try file.writeAll(list.items);
    var file_impl = try std.fs.cwd().createFile("gdextension/gdextension_utility_fn_impl.zig", .{});
    defer file_impl.close();
    try file_impl.writeAll(list_impl.items);
}

/// Added the fn type defined in parsed
/// EG: *const fn([args]) [RT]
fn append_fn_type(parsed: *const std.json.Value, writer: anytype, class_name: ?[]const u8) !void {
    try writer.print("*const fn(", .{});
    // pub const NAME: *const fn(args
    if (class_name) |cn| {
        if (!parsed.object.get("is_static").?.bool) {
            _ = try writer.write("self: ");
            try StructBuilder.write_type_name(cn, writer);
            _ = try writer.write(", ");
        }
    }
    if (parsed.object.get("arguments")) |arguments| {
        try append_fn_args(&arguments, writer, false);
    }
    _ = try writer.write(") ");
    // pub const NAME: *const fn(args) RT
    if (parsed.object.get("return_value")) |return_type| {
        try StructBuilder.write_type_name(return_type.object.get("type").?.string, writer);
    } else if (parsed.object.get("return_type")) |rt| {
        try StructBuilder.write_type_name(rt.string, writer);
    } else {
        _ = try writer.write("void");
    }
}

/// Added all arguments from provided parsed
/// `EG: fieldname[index_number]: ,`
fn append_fn_args(parsed: *const std.json.Value, writer: anytype, append_numbers: bool) !void {
    for (parsed.array.items, 0..) |arg, i| {
        if (append_numbers) {
            try StructBuilder.write_safe_identifier_append_idx(arg.object.get("name").?.string, writer, i);
        } else {
            try StructBuilder.write_safe_identifier(arg.object.get("name").?.string, writer);
        }
        _ = try writer.write(": ");
        try StructBuilder.write_type_name(arg.object.get("type").?.string, writer);
        _ = try writer.write(", ");
    }
}

/// Adds strictyly non-api derived attributes
/// Called a after all other structs have been added and ended
fn type_api_tail_hook(builder: *StructBuilder) !void {
    try builder.start_struct(true, .Const, .Struct, "VariantApi", .{});
    if (CannonVariantTypes) |t| for (t) |vtype| {
        var qsf = QuickStringFmt(64, "to_{s}"){};
        try builder.add_global_field(true, .Var, try qsf.get(.{vtype}), "interface.TypeFromVariantConstructorFunc", "null", .{ .use_safe_type = false });
    };
    try builder.end_struct();
    try builder.start_struct(true, .Const, .Struct, "ObjectApi", .{});
    try builder.add_global_field(true, .Var, "to_variant", "interface.VariantFromTypeConstructorFunc", "null", .{ .use_safe_type = false });
    try builder.end_struct();
}

/// Adds strictly non-api derived attributes
/// Call at tail/(immediately before end_struct)
fn type_api_class_tail_hook(builder: *StructBuilder, class: []const u8) !void {
    _ = class; // autofix
    try builder.add_global_field(true, .Var, "to_variant", "interface.VariantFromTypeConstructorFunc", "null", .{ .use_safe_type = false });
}

fn gen_types(_parsed: *Parsed) !void {
    const parsed = &_parsed.value.object.get("builtin_classes").?;
    var api_buffer = std.ArrayList(u8).init(std.heap.page_allocator);
    defer api_buffer.deinit();
    const api_writer = api_buffer.writer();
    var api_builder = StructBuilder{ .writer = api_writer.any() };
    // var impl_buffer = std.ArrayList(u8).init(std.heap.page_allocator);
    // defer impl_buffer.deinit();
    // const impl_writer = api_buffer.writer();
    // var impl_builder = StructBuilder{ .writer = impl_writer.any() };

    try write_imports(api_writer, .{
        .types = true,
        .gd_const = true,
        .classes = true,
        .interface = true,
    });
    // try write_imports(impl_writer, .{
    //     .types = true,
    //     .gd_const = true,
    //     .classes = true,
    //     .interface = true,
    // });
    for (parsed.array.items) |*class| {
        var qsf = QuickStringFmt(64, "{s}Api"){};
        const class_name = class.object.get("name").?.string; // Class name permitted, because it creates a derivative
        try api_builder.start_struct(true, .Const, .Struct, try qsf.get(.{class_name}), .{});
        try api_builder.print_with_indent("// is_keyed: {any}\n", .{class.object.get("is_keyed").?.bool});

        try gen_types_structs(class, &api_builder); // , &impl_builder

        try api_builder.end_struct();
    }
    try type_api_tail_hook(&api_builder);
    var file = try std.fs.cwd().createFile("gdextension/gdextension_type_api.zig", .{});
    defer file.close();
    try file.writeAll(api_buffer.items);
}

fn gen_types_structs(class: *std.json.Value, api_builder: *StructBuilder) !void {
    // {
    // 	"name": "NodePath",
    // 	"is_keyed": false,
    // 	"operators": [
    // 		{
    // 			"name": "==",
    // 			"right_type": "Variant",
    // 			"return_type": "bool"
    // 		},
    //      ...
    // 	"methods": [
    // 		{
    // 			"name": "get_name",
    // 			"return_type": "StringName",
    // 			"is_vararg": false,
    // 			"is_const": true,
    // 			"is_static": false,
    // 			"hash": 2948586938,
    // 			"arguments": [
    // 				{
    // 					"name": "idx",
    // 					"type": "int"
    // 				}
    // 			]
    // 		},
    //      ...
    // 	],
    // 	"constructors": [
    // 		{
    // 			"index": 1,
    // 			"arguments": [
    // 				{
    // 					"name": "from",
    // 					"type": "NodePath"
    // 				}
    // 			]
    // 		},
    // 	],
    // 	"has_destructor": true
    // },
    if (class.object.get("methods")) |methods| {
        for (methods.array.items) |method| {
            // TODO maybe use the hash as the name to easily init during runtime. Decorate with actual name
            const _method_name = method.object.get("name").?.string;
            var buff2: [128]u8 = undefined;
            const method_name = try std.fmt.bufPrint(&buff2, "@\"{s} {d}\"", .{ _method_name, method.object.get("hash").?.integer });
            try api_builder.add_global_field(true, .Var, method_name, "interface.PtrBuiltInMethod", "null", .{ .use_safe_type = false });
            // ISSUE: gen_function_type generates the correct type based on json
            // but the correct type to pass is interface.PtrBuiltInMethod
            // This applies to constructors as well
            // try gen_function_type(
            //     &method,
            //     writer,
            //     class_name,
            // );
        }
    }
    for (class.object.get("constructors").?.array.items) |ctor| {
        var qsf2 = QuickStringFmt(32, "init{d}"){};
        try api_builder.add_global_field(true, .Var, try qsf2.get(.{ctor.object.get("index").?.integer}), "interface.PtrConstructor", "null", .{ .use_safe_type = false });
    }
    if (!class.object.get("has_destructor").?.bool) {
        try api_builder.write_with_indent("/// This destructor will not exist.\n");
    }
    try api_builder.add_global_field(true, .Var, "deinit", "interface.PtrDestructor", "null", .{ .use_safe_type = false });

    // }
    try type_api_class_tail_hook(api_builder, class.object.get("name").?.string);
}

fn gen_native_structs(parsed: *Parsed, _writer: ?std.io.AnyWriter) !void {
    // {
    // 	"name": "CaretInfo",
    // 	"format": "Rect2 leading_caret;Rect2 trailing_caret;TextServer::Direction leading_direction;TextServer::Direction trailing_direction"
    // },
    // {
    // 	"name": "Glyph",
    // 	"format": "int start = -1;int end = -1;uint8_t count = 0;uint8_t repeat = 1;uint16_t flags = 0;float x_off = 0.f;float y_off = 0.f;float advance = 0.f;RID font_rid;int font_size = 0;int32_t index = 0"
    // },
    // {
    // 	"name": "ObjectID",
    // 	"format": "uint64_t id = 0"
    // },
    const has_writer = _writer != null;
    var file = if (!has_writer) try std.fs.cwd().createFile("gdextension/gdextension_native_structures.zig", .{}) else null;
    var fake_writer = if (!has_writer) std.io.bufferedWriter(file.?.writer()) else null;
    const writer: std.io.AnyWriter = if (has_writer) _writer.? else fake_writer.?.writer().any();
    if (_writer == null) {
        try write_imports(writer, .{
            .types = true,
            .classes = true,
        });
        _ = try writer.write("pub const " ++ ImportConsts.NATIVE ++ " = @This();\n");
    }
    defer if (!has_writer) file.?.close();
    defer if (!has_writer) fake_writer.?.flush() catch unreachable;
    const native = parsed.value.object.get("native_structures").?.array.items;
    var builder = StructBuilder{ .writer = writer };
    for (native) |nstruct| {
        const name = nstruct.object.get("name").?.string;
        const format = nstruct.object.get("format").?.string;
        try builder.start_struct(true, .Const, .Struct, name, .{});
        var iter = std.mem.splitAny(u8, format, ";");
        while (iter.next()) |field| {
            const first_space = std.mem.indexOf(u8, field, " ").?;
            const eql_i = std.mem.indexOf(u8, field, " = ");
            const fname = field[first_space + 1 .. if (eql_i) |e| e else field.len];
            const ftype = field[0..first_space];
            const fdefault = if (eql_i) |e| field[e + 3 ..] else null;

            try builder.add_instance_field(fname, ftype, fdefault, .{});
        }
        try builder.end_struct();
    }
}

fn gen_singletons(parsed: *const Parsed) !void {
    // "global_enums": [
    // 	{
    // 		"name": "Side",
    // 		"is_bitfield": false,
    // 		"values": [
    // 			{
    // 				"name": "SIDE_LEFT",
    // 				"value": 0
    // 			},

    var list = std.ArrayList(u8).init(std.heap.page_allocator);
    const writer = list.writer();
    try write_imports(writer, .{ .classes = true, .types = true, .gd_const = true });
    var builder = StructBuilder{ .writer = writer.any() };
    var buff: [128]u8 = undefined;
    buff[0] = '*';
    for (parsed.value.object.get("singletons").?.array.items) |e| {
        const type_name = e.object.get("type").?.string;
        @memcpy(buff[1 .. type_name.len + 1], type_name);
        try builder.add_global_field(true, .Var, e.object.get("name").?.string, buff[0 .. type_name.len + 1], "undefined", .{});
    }
    var file = try std.fs.cwd().createFile("gdextension/gdextension_singletons.zig", .{});
    defer file.close();
    try file.writeAll(list.items);
}

/// Returns slice to chunk up until the first \n
fn _until_newline(chunk: []const u8) []const u8 {
    for (chunk, 0..) |c, i| {
        if (c == '\n') return chunk[0..i];
    }
    unreachable;
}

/// Parses Interface section of gdextension.h to find names of interface functions
fn parse_gdextension_h(path: []const u8) !void {
    // /**
    //  * @name get_godot_version
    //  * @since 4.1
    //  *
    //  * Gets the Godot version that the GDExtension was loaded into.
    //  *
    //  * @param r_godot_version A pointer to the structure to write the version information into.
    //  */
    // typedef void (*GDExtensionInterfaceGetGodotVersion)(GDExtensionGodotVersion *r_godot_version);
    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
    var out = try std.fs.cwd().createFile("gdextension/gdextension_interface_fns.zig", .{});
    defer out.close();
    var list = std.ArrayList(u8).init(std.heap.page_allocator);
    defer list.deinit();
    var builder = StructBuilder{ .writer = list.writer().any() };
    try write_imports(builder.writer, .{
        .gd_const = true,
        .interface = true,
    });
    const content = try file.readToEndAlloc(std.heap.page_allocator, 2 * 1024 * 1024);
    defer std.heap.page_allocator.free(content);
    var iter = std.mem.splitSequence(u8, content, "* @name ");
    _ = iter.next(); // skip everything before the first
    while (iter.next()) |chunk| {
        if (chunk.len == 0) continue;
        const name = _until_newline(chunk);
        var decl_iter = std.mem.splitSequence(u8, chunk, "typedef ");
        const Docs = decl_iter.next().?;
        { // Woo docs
            var lines = std.mem.splitAny(u8, Docs, "\n");
            try builder.print_with_indent("/// @name {s}\n", .{lines.next().?});
            while (lines.next()) |line| if (line.len > 3) try builder.print_with_indent("/// {s}\n", .{line[3..]});
        }
        const the_type = decl_iter.next().?;
        const start = std.mem.indexOf(u8, the_type, "(*").? + 2;
        const end = std.mem.indexOf(u8, the_type, ")").?;
        const type_name = the_type[start + "GDExtension".len .. end];
        var qsf = QuickStringFmt(256, "interface.{s}"){};
        try builder.add_instance_field(name, try qsf.get(.{type_name}), "null", .{ .use_safe_type = false });
    }
    try out.writeAll(list.items);
}

pub fn gencode(path: []const u8, h_path: []const u8, progress: ?std.Progress.Node) !void {
    try std.io.getStdErr().writer().print("Parsing json...\n", .{});
    var buff = [_]u8{0} ** std.fs.MAX_PATH_BYTES;
    try std.io.getStdErr().writer().print(
        "{s}\n",
        .{try std.fs.realpath(".", &buff)},
    );
    defer if (CannonVariantTypes) |c| std.heap.page_allocator.free(c);
    // Borrowed boiler from thimenesup/GodotZigBindings
    var api_file = try std.fs.cwd().openFile(path, .{});
    defer api_file.close();

    const api_file_buffer = try api_file.readToEndAlloc(std.heap.page_allocator, 1024 * 1024 * 16);
    defer std.heap.page_allocator.free(api_file_buffer);

    var parsed: Parsed = try std.json.parseFromSlice(std.json.Value, std.heap.page_allocator, api_file_buffer, .{});
    defer parsed.deinit();
    const build_config: BuildConfig = .float_64;
    try StructBuilder.init_type_locations(&parsed);

    try gen_type_impl(&parsed, build_config);
    if (progress) |p| p.completeOne();
    try gen_enums(&parsed.value.object.get("global_enums").?, null, &parsed);
    if (progress) |p| p.completeOne();
    try gen_classes(&parsed.value.object.get("classes").?);
    if (progress) |p| p.completeOne();
    try gen_utility_functions(&parsed.value.object.get("utility_functions").?);
    if (progress) |p| p.completeOne();
    try gen_types(&parsed);
    if (progress) |p| p.completeOne();
    try gen_constants(&parsed.value.object.get("header").?, build_config);
    if (progress) |p| p.completeOne();
    try gen_native_structs(&parsed, null);
    if (progress) |p| p.completeOne();
    try gen_singletons(&parsed);
    if (progress) |p| p.completeOne();
    try parse_gdextension_h(h_path);
    if (progress) |p| p.completeOne();
}
