pub const TYPE = "gd_types";
pub const TYPE_API = "gd_type_api";
pub const CLASSES = "gd_classes";
pub const CLASSES_API = "gd_classes_api";
pub const ENUMS = "gd_enums";
pub const UTIL_FUNC = "gd_util_fn";
pub const CONST = "gd_const";
pub const NATIVE = "gd_native_structs";
pub const SINGLETON = "gd_singleton";
pub const INTERFACE = "interface";
pub const API = "gd_api";
