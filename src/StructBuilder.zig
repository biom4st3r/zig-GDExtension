const std = @import("std");
const Parsed = std.json.Parsed(std.json.Value);
const ImportConsts = @import("ImportConsts.zig");
/// Modifiers to how a given function should act.
pub const Opts = struct {
    use_safe_identifier: bool = true,
    use_safe_type: bool = true,
};
writer: std.io.AnyWriter,
indent: u32 = 0,
// Hacky
pub const intention = "    ";
/// Creates proper intention for nested items
fn _write_indent(self: *@This()) !void {
    for (0..self.indent) |_| {
        _ = try self.writer.write(intention);
    }
}
/// Create a enum or struct in the format of:
/// `[pub] const|var _name = struct|enum {\n`
/// Optionally attempted to write a safe name(enforces non-keyword names) via opts flag
pub fn start_struct(self: *@This(), public: bool, mut: enum { Const, Var }, dec_type: enum { Enum, Struct }, _name: []const u8, opts: Opts) !void {
    const name = _name;
    try self._write_indent();
    try self.writer.print("{s}{s} ", .{
        if (public) "pub " else "",
        if (mut == .Var) "var" else "const",
    });
    try self._write_name(name, opts);
    try self.writer.print(" = {s} {{\n", .{if (dec_type == .Enum) "enum(u32)" else "extern struct"});
    self.indent += 1;
}
pub fn end_struct(self: *@This()) !void {
    self.indent -= 1;
    try self._write_indent();
    _ = try self.writer.write("};\n");
}
/// Adds a enum field in the form of:
/// `name[ = default_value],\n`
/// Optionally writes attmpts to write safe name based on opts flag
pub fn add_enum_field(self: *@This(), name: []const u8, default_value: ?i64, opts: Opts) !void {
    try self._write_indent();
    try self._write_name(name, opts);
    if (default_value) |val| {
        try self.writer.print(" = {d},\n", .{val});
    } else {
        _ = try self.writer.write(",\n");
    }
}
/// Creates a local or global variable with the start of function type in the form of:
/// `[pub ][const ]name: *const fn (`
/// Should be used with `add_fn_arg` and `end_fn_args`
pub fn start_fn_type(self: *@This(), public: bool, scope: enum { Instance, Global }, mut: enum { Const, Var }, name: []const u8, opts: Opts) !void {
    switch (scope) {
        .Instance => {
            try self._write_name(name, opts);
        },
        .Global => {
            if (public) _ = try self.writer.write("pub ");
            _ = try self.writer.write(if (mut == .Var) "var " else "const ");
            try self._write_name(name, opts);
        },
    }
    _ = try self.writer.write(": *const fn (");
}
/// Suffixes the declation of a global or local variable that has a function type
/// ` = null|undefined;`
pub fn end_fn_type(self: *@This(), value: []const u8) !void {
    try self.writer.print(" = {s};\n", .{value});
}
/// Fully creates the type used for constructors
/// `*const fn(self: *SomeType, args: [*c]*const anyopaque) void`
pub fn add_ctor_fn_type(self: *@This(), self_type: []const u8, opts: Opts) !void {
    _ = try self.writer.write("*const fn(self: *");
    self._write_type(self_type, opts);
    _ = try self.writer.write(", args: [*c]*const anyopaque) void");
}
/// Fully creates the type used for deconstructors
/// `?*const fn(self: *SomeType) void`
pub fn add_dctor_func_type(self: *@This(), self_type: []const u8, opts: Opts) !void {
    _ = try self.writer.write("?*const fn(self: *");
    self._write_type(self_type, opts);
    _ = try self.writer.write(") void");
}
/// If a type name is not provided this will stop short at
/// `pub const TYPENAME: ` ignoring value;
/// Otherwise it will create the form:
/// `[pub ]const|var name: _type_name = value|undefined;\n`
pub fn add_global_field(self: *@This(), public: bool, mut: enum { Const, Var }, name: []const u8, _type_name: ?[]const u8, value: []const u8, opts: Opts) !void {
    try self._write_indent();
    if (public) _ = try self.writer.write("pub ");
    switch (mut) {
        .Const => _ = try self.writer.write("const "),
        .Var => _ = try self.writer.write("var "),
    }
    try self._write_name(name, opts);
    _ = try self.writer.write(": ");
    if (_type_name) |type_name| {
        try self._write_type(type_name, opts);
        try self.writer.print(" = {s};\n", .{value});
    }
}
pub fn _write_name(self: *@This(), name: []const u8, opts: Opts) !void {
    if (opts.use_safe_identifier) try write_safe_identifier(name, self.writer) else _ = try self.writer.write(name);
}
pub const MemberTypes = enum {
    float,
    double,
    int32,
    int,
    // bool,
    int32_t,
    uint8_t,
    uint16_t,
    real_t,
    uint64_t,

    Int,
    Float,
    Bool,
};
pub const MemberTypeMapping = [_][]const u8{
    "f64", // This might depend on the build_configuration
    "f64",
    "i32",
    "i64",
    // "bool",
    "i32",
    "u8",
    "u16",
    "f64", // this might be wrong.
    "u64",

    "i64",
    "f64",
    "bool",
};
pub fn get_member_type(member: std.json.Value) []const u8 {
    const member_type = member.object.get("meta").?.string;
    const value = std.meta.stringToEnum(MemberTypes, member_type);
    if (value) |v| {
        return MemberTypeMapping[@intFromEnum(v)];
    } else {
        return member_type;
    }
    return;
}

fn _write_type(self: *@This(), type_name: []const u8, opts: Opts) !void {
    if (opts.use_safe_type) try write_type_name(type_name, self.writer) else if (std.meta.stringToEnum(MemberTypes, type_name)) |t| {
        // Always atleast use Mapping to avoid int,float, bool
        _ = try self.writer.write(MemberTypeMapping[@intFromEnum(t)]);
    } else {
        _ = try self.writer.write(type_name);
    }
}
/// Creates an instance_field intended for a struct in the form of:
/// `name: field_type[ = _default_value],\n`
/// Special handling for several c style naming such as:
/// *name, 0.f, name[array_size]
/// ______
pub fn add_instance_field(
    self: *@This(),
    _name: []const u8,
    field_type: []const u8,
    _default_value: ?[]const u8,
    opts: Opts,
) !void {
    var array_size: ?[]const u8 = null;
    var name = _name;
    var default_value = _default_value;
    if (default_value) |val| {
        if (std.mem.eql(u8, val, "0.f")) {
            default_value = "0.0";
        }
    }
    try self._write_indent();
    if (name[0] == '*') {
        try _write_name(self, name[1..], opts);
    } else if (name[name.len - 1] == ']') { // int arr[32];
        const i = std.mem.indexOf(u8, name, "[").?;
        array_size = name[i..];
        name = name[0..i];
        try _write_name(self, name, opts);
    } else {
        try _write_name(self, name, opts);
    }
    _ = try self.writer.write(": ");
    if (name[0] == '*') {
        _ = try self.writer.write("*");
    }
    if (array_size) |size| {
        _ = try self.writer.write(size);
    }
    try _write_type(self, field_type, opts);
    if (default_value) |val| {
        try self.writer.print(" = {s}", .{val});
    }
    _ = try self.writer.write(",\n");
}
pub const ArgOpts = struct {
    is_const: bool = false,
    is_ptr: bool = false,
    arg_type: []const u8,
    append_number: ?u64 = null,
    is_slice: bool = false,
};
/// Start the declaration of a function in the form off
/// `[pub ]fn name([self: self_opts.arg_type, ]`
/// Reacts to ArgOpts.is_const and is_ptr
pub fn start_fn(self: *@This(), vis: bool, name: []const u8, self_opts: ?ArgOpts, opts: Opts) !void {
    try self._write_indent();
    if (vis) _ = try self.writer.write(if (vis) "pub fn " else "fn ");
    try self._write_name(name, opts);
    _ = try self.writer.write(" (");
    if (self_opts) |selfo| { //          *const TYPE
        try self.writer.print("self: {s}{s}", .{ if (selfo.is_ptr) "*" else "", if (selfo.is_const) "const " else "" });
        try self._write_type(selfo.arg_type, opts);
        _ = try self.writer.write(", ");
    }
    self.indent += 1;
}
pub fn end_fn(self: *@This()) !void {
    self.indent -= 1;
    try self._write_indent();
    _ = try self.writer.write("}\n");
}

/// Intended to add an argument after the openning bracket of a function or function type in the form of:
/// `name: type[, ]`
/// _____
/// reacts to ArgOpts.append_number by appending number to argument name.
/// reacts to ArgOpts.is_slice by making this arg a const slice type `[]const TPYE`
pub fn add_fn_arg(self: *@This(), name: []const u8, arg: ArgOpts, opts: Opts, last_arg: bool) !void {
    try self._write_name(name, opts);
    if (arg.append_number) |i| {
        try self.writer.print("{d}", .{i});
    }
    // _ = try self.writer.write(": ");
    try self.writer.print(": {s}{s}{s}", .{
        if (arg.is_slice) "[]const " else "",
        if (arg.is_ptr) "*" else "",
        if (arg.is_const) "const " else " ",
    });
    try self._write_type(arg.arg_type, opts);
    if (!last_arg) _ = try self.writer.write(", ");
}
/// Intended to conclude add_fn_arg. Leaves the form:
/// `[pub ]fn fn_name([self: *HostType, ]args...) {\n`
pub fn end_fn_args(self: *@This(), return_type: []const u8, opts: Opts) !void {
    _ = try self.writer.write(") ");
    try self._write_type(return_type, opts);
    _ = try self.writer.write(" {\n");
}

pub fn write_with_indent(self: *@This(), buff: []const u8) !void {
    try self._write_indent();
    _ = try self.writer.write(buff);
}

pub fn print_with_indent(self: *@This(), comptime fmt: []const u8, args: anytype) !void {
    try self._write_indent();
    try self.writer.print(fmt, args);
}

const UnsafeNames = enum {
    @"align",
    @"enum",
    @"error",
    @"var",
    bool,
    type,
};

const SafeName = [_][]const u8{
    "@\"align\"",
    "@\"enum\"",
    "@\"error\"",
    "@\"var\"",
    "@\"bool\"",
    "@\"type\"",
};

pub fn get_safe_identifier(t: []const u8) []const u8 {
    if (std.meta.stringToEnum(UnsafeNames, t)) |e| {
        return SafeName[@intFromEnum(e)];
    } else {
        return t;
    }
}

pub fn write_safe_identifier(t: []const u8, writer: anytype) !void {
    if (std.meta.stringToEnum(UnsafeNames, t)) |e| {
        _ = try writer.write(SafeName[@intFromEnum(e)]);
    } else {
        _ = try writer.write(t);
    }
}
pub fn write_safe_identifier_append_idx(t: []const u8, writer: anytype, idx: u64) !void {
    if (std.meta.stringToEnum(UnsafeNames, t)) |e| {
        _ = try writer.write(SafeName[@intFromEnum(e)][0 .. SafeName[@intFromEnum(e)].len - 1]);
        try writer.print("{d}\"", .{idx});
    } else {
        try writer.print("{s}{d}", .{ t, idx });
    }
}
pub var typeLoc = std.StringHashMap([]const u8).init(std.heap.page_allocator);
pub fn init_type_locations(parsed: *Parsed) !void {
    for (parsed.value.object.get("classes").?.array.items) |e| {
        try typeLoc.put(e.object.get("name").?.string, ImportConsts.CLASSES);
    }
    for (parsed.value.object.get("builtin_class_sizes").?.array.items[0].object.get("sizes").?.array.items) |class| {
        try typeLoc.put(class.object.get("name").?.string, ImportConsts.TYPE);
    }
    for (parsed.value.object.get("builtin_class_member_offsets").?.array.items[0].object.get("classes").?.array.items) |class| {
        try typeLoc.put(class.object.get("name").?.string, ImportConsts.TYPE);
    }
    for (parsed.value.object.get("global_enums").?.array.items) |e| {
        try typeLoc.put(e.object.get("name").?.string, ImportConsts.ENUMS);
    }
    for (parsed.value.object.get("native_structures").?.array.items) |e| {
        try typeLoc.put(e.object.get("name").?.string, ImportConsts.NATIVE);
    }
    _ = typeLoc.remove("bool");
    _ = typeLoc.remove("int");
    _ = typeLoc.remove("float");
}

pub fn write_type_name(t: []const u8, writer: anytype) !void {
    // "type": "enum::AESContext.Mode"
    // PhysicsServer3DExtensionMotionCollision collisions[32]
    const value = std.meta.stringToEnum(MemberTypes, t);
    const enum_index: ?[]const u8 = if (std.mem.indexOf(u8, t, "enum::")) |_| "enum::" else if (std.mem.indexOf(u8, t, "bitfield::")) |_| "bitfield::" else null;
    if (value) |v| {
        try writer.print("{s}", .{MemberTypeMapping[@intFromEnum(v)]});
    } else if (enum_index) |header| {
        if (std.mem.indexOf(u8, t, ".")) |class_dot| {
            try write_type_name(t[header.len..class_dot], writer);
            _ = try writer.write(t[class_dot..]);
        } else {
            try writer.print("gd_enums.{s}", .{t[header.len..]});
        }
    } else if (typeLoc.get(t)) |found| {
        try writer.print("{s}.{s}", .{ found, t });
    } else if (std.mem.indexOf(u8, t, "typedarray::")) |_| {
        _ = try writer.write(ImportConsts.CONST ++ ".typed_array(");
        try write_type_name(t["typedarray::".len..], writer);
        _ = try writer.write(")");
    } else if (std.mem.indexOf(u8, t, "::")) |enum_ref| {
        try write_type_name(t[0..enum_ref], writer);
        try writer.print(".{s}", .{t[enum_ref + 2 ..]});
    } else if (std.mem.eql(u8, t[t.len - 2 ..], "**")) {
        try writer.print("[*c][*c]", .{});
        try write_type_name(t[0 .. t.len - 3], writer);
    } else if (t[t.len - 1] == '*') {
        try writer.print("*", .{});
        try write_type_name(t[0 .. t.len - 1], writer);
    } else if (std.mem.indexOf(u8, t, "const ")) |_| {
        try writer.print("const ", .{});
        try write_type_name(t["const ".len..], writer);
    } else if (t[0] == '*') {
        _ = try writer.write("*");
        try write_type_name(t[1..], writer);
    } else if (!std.ascii.isAlphabetic(t[0])) {
        // How to properly handle: TODO
        // https://github.com/godotengine/godot-headers/issues/67#issuecomment-927620531
        std.log.err("Invalid type name {s}. Handling it...", .{t});
        if (std.mem.indexOf(u8, t, ":")) |i| {
            try write_type_name(t[i + 1 ..], writer);
        } else {
            @panic("JK. I couldn't handle it.");
        }
    } else {
        try writer.print(ImportConsts.CONST ++ ".type_assumed({s})", .{t});
    }
}
