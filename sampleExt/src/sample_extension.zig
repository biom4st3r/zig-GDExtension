const std = @import("std");

const Something = ?*anyopaque;
const gd = @import("gdapi");

pub fn extension() void {}

pub var get_proc: gd.interface.InterfaceGetProcAddress = null;
pub var class_library: gd.interface.ClassLibraryPtr = null;

pub fn initalize(userdata: Something, p_level: gd.interface.InitializationLevel) callconv(.C) void {
    std.debug.print("Hello world @ {d}\n", .{p_level});
    if (p_level == gd.interface.InitLevels.EDITOR) {
        gd.init(get_proc, class_library) catch unreachable; // autofix
        @import("Tests.zig").init();
    }
    _ = userdata; // autofix
}
pub fn deinitalize(userdata: Something, p_level: gd.interface.InitializationLevel) callconv(.C) void {
    std.debug.print("Goodbye world @ {d}\n", .{p_level});
    _ = userdata; // autofix
}

export fn hello_extension_entry(
    p_get_proc_address: gd.interface.InterfaceGetProcAddress,
    p_library: gd.interface.ClassLibraryPtr,
    r_init: *gd.interface.Initialization,
) bool {
    get_proc = p_get_proc_address;
    class_library = p_library;
    r_init.initialize = initalize;
    r_init.deinitialize = deinitalize;
    return true;
}
