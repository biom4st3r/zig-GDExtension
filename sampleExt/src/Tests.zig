const API = @import("gdapi");
const interface = API.interface;
const std = @import("std");
const enums = API.enums;
const types = API.types;
const convert = API.convert;
const Zig2Gd = convert.Zig2Godot;
const Gd2Zig = convert.Godot2Zig;
const reg = API.register;
const classes = API.classes;

pub const GDPackedByteArray = extern struct {
    //Vector<uint8_ti>
    ptr: [*]u8,
    size: usize,
};

pub fn init() void {
    reg.register_class(TESTY_TEST, classes.Node, null);
    // const obj = create_an_object();
    // defer API.fns.mem_free.?(obj);
}
pub fn create_an_object() *types.Object {
    var obj_name = convert.to_stringname("Node");
    defer obj_name.deinit();
    var obj: *types.Object = @ptrCast(@alignCast(API.fns.classdb_construct_object.?(&obj_name)));
    _ = &obj;
    // API.fns.object_set_instance.?(obj, &obj_name, null);
    return obj;
}
const TESTY_TEST = struct {
    fn static_noarg_noret() void {
        std.log.debug("Hello Workd!", .{});
    }
    fn static_arg_noret(i: i64) void {
        std.log.debug("maybe_a_numbers {d}", .{i});
    }
    fn static_noarg_ret() i64 {
        return 12;
    }
    fn static_print_variant(variant: *types.Variant) void {
        API.util_fns.general.print(&.{variant});
    }
    fn static_print_string(string: types.String) void {
        API.util_fns.general.print(&.{&string.to_variant().?});
    }
    fn static_print_type(variant: *types.Variant) void {
        const t: types.Variant.Type = @enumFromInt(API.fns.variant_get_type.?(variant));
        std.log.debug("{s}", .{@tagName(t)});
    }
    f: i64 = 0,
    pub fn get_f(self: *@This()) i64 {
        return self.f;
    }
    pub fn set_f(self: *@This(), f: i64) void {
        self.f = f;
    }
    fn instance_noarg_noret(self: *@This()) void {
        std.log.debug("print_f {d}", .{self.f});
    }
    fn on_ready(self: *@This()) void {
        _ = self; // autofix
        std.log.debug("called virtual function `_ready`", .{});
    }
    pub fn get(self: *@This(), name: []const u8, out: *types.Variant) bool {
        std.log.debug("getting {s}", .{name});
        _ = self; // autofix
        _ = out; // autofix
        return false;
    }
    pub fn set(self: *@This(), name: []const u8, val: *const types.Variant) bool {
        std.log.debug("setting {s}", .{name});
        _ = self; // autofix
        _ = val; // autofix
        return false;
    }
    pub fn get_virtual(self: *@This(), name: []const u8) interface.ClassCallVirtual {
        if (std.mem.eql(u8, name, "_ready")) {
            return convert.to_ClassCallVirtual(TESTY_TEST, on_ready);
        }
        _ = self; // autofix
        std.log.debug("getting virtual fn {s}", .{name});
        return null;
    }
    pub fn get_rid(self: *@This()) u64 {
        std.log.debug("getting rid {d}", .{@intFromPtr(self)});
        return @intFromPtr(self);
    }
    pub fn notification(self: *@This(), what: i32) void {
        const noti = std.meta.intToEnum(enums.Notification, what) catch {
            std.log.debug("received unknown notification {d}", .{what});
            return;
        };
        _ = self; // autofix
        std.log.debug("received notification {s}", .{@tagName(@as(enums.Notification, noti))});
    }
    pub fn reference(self: *@This()) void {
        _ = self; // autofix
        std.log.debug("referenced!", .{});
    }
    pub fn unreference(self: *@This()) void {
        _ = self; // autofix
        std.log.debug("unreferenced!", .{});
    }
    pub fn to_string(self: *@This(), is_valid: *bool, out: *types.String) void {
        _ = self; // autofix
        is_valid.* = true;
        out.* = Zig2Gd.string("TESTY_TEST Stirng!");
    }

    pub fn bind_methods(class: types.StringName) void {
        _ = class; // autofix
        reg.register_method(TESTY_TEST, "static_noarg_noret", TESTY_TEST.static_noarg_noret, .{});
        reg.register_method(TESTY_TEST, "static_arg_noret", TESTY_TEST.static_arg_noret, .{ .arg_names = &.{"an_int"} });
        reg.register_method(TESTY_TEST, "static_noarg_ret", TESTY_TEST.static_noarg_ret, .{});
        reg.register_method(TESTY_TEST, "instance_noarg_noret", TESTY_TEST.instance_noarg_noret, .{});
        reg.register_method(TESTY_TEST, "static_print_variant", TESTY_TEST.static_print_variant, .{});
        reg.register_method(TESTY_TEST, "static_print_string", TESTY_TEST.static_print_string, .{});
        reg.register_method(TESTY_TEST, "static_print_type", TESTY_TEST.static_print_type, .{});
    }

    pub fn bind_properties(class: types.StringName) void {
        _ = class; // autofix
        reg.register_property(TESTY_TEST, @TypeOf(@field(TESTY_TEST{}, "f")), "f", .{ .Generate = {} }, .{ .Generate = {} }, "Testing property hint string");
    }
    pub fn bind_enums(class: types.StringName) void {
        _ = class; // autofix
    }
    pub fn bind_signals(class: types.StringName) void {
        _ = class; // autofix
    }
};
