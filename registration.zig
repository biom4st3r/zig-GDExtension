const API = @import("api.zig");
const interface = API.interface;
const std = @import("std");
const enums = API.enums;
const types = API.types;
const convert = API.convert;
const Godot2Zig = convert.Godot2Zig;
const Zig2Godot = convert.Zig2Godot;

// FIXME: Replace this global allocator with some other method
pub var GPA = std.heap.GeneralPurposeAllocator(.{}){};
pub var alloc = GPA.allocator();

pub fn find_class(T: type) type {
    return ExtensionClass(T, null);
}

pub fn ExtensionClass(T_ClassType: type, T_ClassUserData: ?type) type {
    const outT = struct {
        pub var SUPER: types.StringName = undefined;
        pub var CLASSNAME: types.StringName = undefined;
        pub const ClassType: type = T_ClassType;
        pub const ClassUserData: type = if (T_ClassUserData) |t| ?t else ?*anyopaque;
        pub const Self = @This();
        obj: *types.Object = undefined,
        wrapped: ClassType = undefined,

        fn create() !*Self {
            return try alloc.create(Self);
        }
        fn init_gdobject(self: *@This()) void {
            self.obj = @ptrCast(@alignCast(API.fns.classdb_construct_object.?(&SUPER)));
            API.fns.object_set_instance.?(@ptrCast(self.obj), &CLASSNAME, @ptrCast(&self.wrapped));
        }
        /// Provides an instance of ClassType back to godot wrapped in an initlized types.Object
        pub fn create_instance(userdata: ClassUserData) callconv(.C) *types.Object {
            if (std.meta.hasMethod(ClassType, "create_instance")) {
                return ClassType.create_instance(@ptrCast(userdata));
            }
            var t = create() catch unreachable;
            t.init_gdobject();

            // initlize classType
            t.wrapped = .{};
            return t.obj;
        }
        /// Frees any allocations the classType object has made, including the object itself
        pub fn free_instance(userdata: ClassUserData, self: ?*ClassType) callconv(.C) void {
            if (std.meta.hasMethod(ClassType, "free_instance")) {
                return ClassType.free_instance(@ptrCast(userdata), self);
            }
            const extension: *Self = @alignCast(@fieldParentPtr("wrapped", self.?));
            alloc.destroy(extension);
        }
        /// Gets the value of a property by property StringName.
        pub fn get(self: *ClassType, _name: *const types.StringName, out_value: *types.Variant) callconv(.C) interface.Bool {
            if (types.StringName.is_empty(@constCast(_name))) {
                std.log.info("Tried to `get()` empty stringname on {s}\n", .{@typeName(ClassType)});
            }
            if (comptime std.meta.hasMethod(ClassType, "get")) {
                var buff: [128]u8 = undefined;
                const name = Godot2Zig.stringName(_name, &buff);
                return if (self.get(name, out_value)) 1 else 0;
            }

            // std.log.debug("Unhandled `get()` on {s}", .{@typeName(ClassType)});
            // API.util_fns.general.print(&.{&string.to_variant().?});
            return 0;
        }
        pub fn set(self: *ClassType, _name: *const types.StringName, value: *const types.Variant) callconv(.C) interface.Bool {
            if (types.StringName.is_empty(@constCast(_name))) {
                std.log.info("Tried to `set()` empty stringname on {s}\n", .{@typeName(ClassType)});
            }
            if (comptime std.meta.hasMethod(ClassType, "set")) {
                var buff: [128]u8 = undefined;
                const name = Godot2Zig.stringName(_name, &buff);
                return if (self.set(name, value)) 1 else 0;
            }

            // std.log.debug("Unhandled `set()` on {s}", .{@typeName(ClassType)});
            // API.util_fns.general.print(&.{&string.to_variant().?});
            return 0;
        }
        pub fn to_string(self: *ClassType, is_valid: *bool, out: *types.String) callconv(.C) void {
            if (std.meta.hasMethod(ClassType, "to_string")) {
                // TODO maybe provide multiple entrypoints to_string_raw[passes out], to_string_noalloc[no allocator], to_string_alloc[passes allocator];
                self.to_string(is_valid, out);
                return;
            }
            is_valid.* = true;
            out.* = types.String.init2(CLASSNAME);
        }
        /// Notifies the object that a new reference to `self` has been created. useful for ref counting
        pub fn reference(self: *ClassType) callconv(.C) void {
            if (std.meta.hasMethod(ClassType, "reference")) {
                self.reference();
                return;
            }
        }
        /// Notifies the object that a reference to `self` has been destroyed
        pub fn unreference(self: *ClassType) callconv(.C) void {
            if (std.meta.hasMethod(ClassType, "unreference")) {
                self.unreference();
                return;
            }
        }
        pub fn notification(self: *ClassType, what: i32) callconv(.C) void {
            if (std.meta.hasMethod(ClassType, "notification")) {
                self.notification(what);
                return;
            }
        }
        pub fn get_virtual(self: *ClassType, _name: *const types.StringName) callconv(.C) interface.ClassCallVirtual {
            if (comptime std.meta.hasMethod(ClassType, "get_virtual")) {
                var buff: [128]u8 = undefined;
                const name = Godot2Zig.stringName(_name, &buff);
                return self.get_virtual(name);
            }
            return null;
        }
        pub fn get_rid(self: *ClassType) callconv(.C) u64 {
            if (comptime std.meta.hasMethod(ClassType, "get_rid")) {
                return self.get_rid();
            }
            return @intFromPtr(self);
        }
        // TODO Add the rest of the methods
    };
    return outT;
}

fn to_methodarg_metadata(T: type) interface.ClassMethodArgumentMetadata {
    const info = @typeInfo(T);
    switch (info) {
        .Int => |i| {
            switch (i.signedness) {
                .signed => {
                    return switch (i.bits) {
                        1...8 => interface.MethodArgumentMetadata.INT_IS_INT8,
                        9...16 => interface.MethodArgumentMetadata.INT_IS_INT16,
                        17...32 => interface.MethodArgumentMetadata.INT_IS_INT32,
                        33...64 => interface.MethodArgumentMetadata.INT_IS_INT64,
                        else => @compileError("Integer gt 64bit is not compatibile with GDExtension"),
                    };
                },
                .unsigned => {
                    return switch (i.bits) {
                        1...8 => interface.MethodArgumentMetadata.INT_IS_UINT8,
                        9...16 => interface.MethodArgumentMetadata.INT_IS_UINT16,
                        17...32 => interface.MethodArgumentMetadata.INT_IS_UINT32,
                        33...64 => interface.MethodArgumentMetadata.INT_IS_UINT64,
                        else => @compileError("Integer gt 64bit is not compatibile with GDExtension"),
                    };
                },
            }
        },
        .Float => |f| {
            switch (f.bits) {
                32 => return interface.MethodArgumentMetadata.REAL_IS_FLOAT,
                64 => return interface.MethodArgumentMetadata.REAL_IS_DOUBLE,
                else => @compileError("Zig supports custom bit floats?"),
            }
        },
        else => return interface.MethodArgumentMetadata.NONE,
    }
}
fn to_string(i: u64) [:0]const u8 {
    return switch (i) {
        0 => "zero",
        1 => "one",
        2 => "two",
        3 => "three",
        4 => "four",
        5 => "five",
        else => {
            @panic("To Many Args");
        },
    };
}

pub const Encap = enum {
    None,
    Generate,
    Provided,
};

pub const EncapFunction = union(Encap) {
    /// No [gs]etter is registered or should be registered
    None: void,
    /// Generate and register a [gs]etter for me with the name `"[sg]et_" ++ prop_name`
    Generate: void,
    /// A [gs]etter is already registered under this name
    Provided: [:0]const u8,
};

pub fn register_property(
    class_type: type,
    field_type: type,
    comptime prop_name: [:0]const u8,
    _setter: EncapFunction,
    _getter: EncapFunction,
    hint_string: [:0]const u8,
) void {
    var setter: ?types.StringName = null;
    var getter: ?types.StringName = null;
    // I don't trust break :label value. It already bugs out the stack traces
    switch (_setter) {
        .None => |_| {
            setter = null;
        },
        .Generate => |_| {
            if (!std.meta.hasMethod(class_type, "set_" ++ prop_name)) {
                @compileError("Requests generate setter for `" ++ prop_name ++ "` but " ++ "`set_" ++ prop_name ++ "` doesn't exist in " ++ @typeName(class_type));
            }
            setter = Zig2Godot.stringName("set_" ++ prop_name);
            register_method(class_type, "set_" ++ prop_name, struct {
                fn set(self: *class_type, value: field_type) void {
                    @field(self, prop_name) = value;
                }
            }.set, .{ .arg_names = &.{"val"} });
        },
        .Provided => |s| {
            setter = Zig2Godot.stringName(s);
        },
    }
    defer if (setter) |s| s.deinit();
    switch (_getter) {
        .None => {
            setter = null;
        },
        .Generate => {
            if (!std.meta.hasMethod(class_type, "get_" ++ prop_name)) {
                @compileError("Requests generate getter for `" ++ prop_name ++ "` but " ++ "`get_" ++ prop_name ++ "` doesn't exist in " ++ @typeName(class_type));
            }
            getter = Zig2Godot.stringName("get_" ++ prop_name);
            register_method(class_type, "get_" ++ prop_name, struct {
                fn get(self: *class_type) field_type {
                    return @field(self, prop_name);
                }
            }.get, .{ .arg_names = null });
        },
        .Provided => |s| {
            getter = Zig2Godot.stringName(s);
        },
    }
    defer if (getter) |s| s.deinit();
    var classname = find_class(class_type).CLASSNAME;
    var propname = Zig2Godot.stringName(prop_name);
    defer propname.deinit();
    var hintstring = Zig2Godot.stringName(hint_string);
    defer hintstring.deinit();
    API.fns.classdb_register_extension_class_property.?(
        API.class_library,
        &classname,
        @ptrCast(&interface.PropertyInfo{
            .type = @intFromEnum(Zig2Godot.typing_enum(field_type) catch @panic("CAnnot ignore erorr3")),
            .name = @ptrCast(&propname),
            .class_name = @ptrCast(&classname),
            .hint = @intFromEnum(enums.PropertyHint.PROPERTY_HINT_NONE),
            .hint_string = @ptrCast(&hintstring),
            .usage = enums.PropertyUsageFlags.PROPERTY_USAGE_NONE,
        }),
        @ptrCast(&setter),
        @ptrCast(&getter),
    );
}

fn __is_static(fninfo: std.builtin.Type.Fn, self_type: type) bool {
    if (fninfo.params.len == 0) return true else {
        const arg0 = fninfo.params[0];
        const is_self = self_type == arg0.type;
        if (is_self) @compileError("Must be pointer to self type, Not copy of.");
        if (arg0.type == null) @compileError("params[0].type cannot be null");
        return unwrap_pointer(arg0.type.?) != self_type;
    }
}

fn after_x(string: [:0]const u8, c: u8) if (@inComptime()) ?[:0]const u8 else ?[]const u8 {
    const i = std.mem.lastIndexOf(u8, string, &[1]u8{c});
    if (i) |ii| return string[ii + 1 ..];
    return null;
}

/// Registrers a class and it's parts to be used in Godot
///  ============================
/// `class` - the struct type you are registering
/// `superclass` - an already registered type.
///     * This really just uses the name of the provided type.
///     * For builtin types it's recommended to use `gdapi.classes`
/// =============================
/// Reserved Function Names:
/// Creating a function with any of these names will overwrite the api provided implementation.
/// `set` - for godot. Called anytime a value is set
/// `get` - for godot. Called anytype a value is feteched
/// `notification` - for godot. When notifications are sent to your object. See `gdapi.global_enums.Notifications` though it is not exhaustive
/// `to_string` - for godot.
/// `reference` - for godot. Called when a new reference is created. For refcounting
/// `unreference` - for godot. See above
/// `create_instance` - for godot. Converts a `class` instance into a godot object.
/// `free_instance` - for godot. Return the godot object to be freed if nessisary
/// `get_virtual` - for godot. Return a virtual function call to godot.
/// `get_rid` - for godot. Returns the resource id for this object. Default implementation is ptr address.
/// =======================
/// `bind_methods` - for API. Called after registering classes to bind any method for the struct. See `register_method`.
/// `bind_properties` - for API. see above
/// `bind_signals` - for API. see above above
pub fn register_class(class: type, superclass: type, user_data: ?*anyopaque) void {
    const class_name = get_class_name(class);
    const super_name = get_class_name(superclass);
    const EType = ExtensionClass(class, null);
    EType.CLASSNAME = Zig2Godot.stringName(class_name);
    EType.SUPER = Zig2Godot.stringName(super_name);

    const class_info = interface.ClassCreationInfo{
        .is_virtual = 0,
        .is_abstract = 0,
        // Set a field by name
        .set_func = @ptrCast(&EType.set),
        // Get a field by name
        .get_func = @ptrCast(&EType.get),
        .get_property_list_func = null,
        .free_property_list_func = null,
        .property_can_revert_func = null,
        .property_get_revert_func = null,
        .notification_func = @ptrCast(&EType.notification),
        .to_string_func = @ptrCast(&EType.to_string),
        // Called when a new reference to an instance is created
        .reference_func = @ptrCast(&EType.reference),
        // Called when a reference to an instance is destroyed
        .unreference_func = @ptrCast(&EType.unreference),
        .create_instance_func = @ptrCast(&EType.create_instance),
        .free_instance_func = @ptrCast(&EType.free_instance),
        .get_virtual_func = @ptrCast(&EType.get_virtual),
        .get_rid_func = @ptrCast(&EType.get_rid),
        .class_userdata = user_data, // user_data,
    };
    API.fns.classdb_register_extension_class.?(
        API.class_library,
        @ptrCast(&EType.CLASSNAME),
        @ptrCast(&EType.SUPER),
        &class_info,
    );
    // if (true) return;
    if (std.meta.hasFn(class, "bind_methods")) class.bind_methods(EType.CLASSNAME);
    if (std.meta.hasFn(class, "bind_properties")) class.bind_properties(EType.CLASSNAME);
    if (std.meta.hasFn(class, "bind_signals")) class.bind_signals(EType.CLASSNAME);
}

fn unwrap_pointer(T: type) type {
    switch (@typeInfo(T)) {
        .Pointer => |p| return unwrap_pointer(p.child),
        else => return T,
    }
}

fn unwrap_type_name(T: type) [:0]const u8 {
    return @typeName(unwrap_pointer(T));
}

fn get_class_name(T: type) [:0]const u8 {
    return after_x(unwrap_type_name(T), '.').?;
}

fn __propinfo_len(func: anytype) usize {
    return @typeInfo(unwrap_pointer(@TypeOf(func))).Fn.params.len;
}

fn function_to_propinfo_deinit(props: []interface.PropertyInfo) void {
    for (props) |prop| {
        @as(*types.StringName, @ptrCast(@alignCast(prop.class_name.?))).deinit();
        if (prop.name) |name| {
            @as(*types.StringName, @ptrCast(@alignCast(name))).deinit();
        }
    }
}
/// Must be inline stack pointers are used.
inline fn function_to_propinfo(
    comptime is_static: bool,
    func: anytype,
    arg_names: ?[]const ?[:0]const u8,
) [__propinfo_len(func) - if (is_static) 0 else 1]interface.PropertyInfo {
    const offset = if (is_static) 0 else 1;
    const params = @typeInfo(unwrap_pointer(@TypeOf(func))).Fn.params;
    var properties: [params.len - offset]interface.PropertyInfo = undefined;
    inline for (params[offset..], &properties, 0..) |param, *prop, i| {
        prop.* = .{};
        if (param.is_generic) @compileError("anytype arguments are not compatible with GDExtension.");
        const gdtype = comptime Zig2Godot.typing_enum(param.type.?) catch types.Variant.Type.Object; //FIXME: don't just hardcode Object...
        const has_name = arg_names != null and arg_names.?[i] != null;
        std.log.debug("Param '{d}' of type '{s}' converts to {s} named `{s}`", .{ i, @typeName(param.type.?), @tagName(gdtype), if (has_name) arg_names.?[i].? else "No Name" });
        // Cannot be null
        var name: types.StringName = undefined;
        if (has_name) {
            std.log.debug("arg '{d}' hint '{s}'", .{ i, arg_names.?[i].? });
            name = Zig2Godot.stringName(arg_names.?[i].?);
        } else {
            name = Zig2Godot.stringName("");
        }
        // defer name.deinit();

        var cn: types.StringName = undefined;
        if (gdtype == .Object) {
            cn = Zig2Godot.stringName(after_x(@typeName(param.type.?), '.'));
        } else {
            cn = Zig2Godot.stringName("");
        }

        // defer cn.deinit();
        prop.* = .{
            .type = @intFromEnum(gdtype),
            .name = @ptrCast(&name),
            .class_name = @ptrCast(&cn),
            .hint = @intFromEnum(enums.PropertyHint.PROPERTY_HINT_NONE),
            .hint_string = @ptrCast(&name),
            .usage = enums.PropertyUsageFlags.PROPERTY_USAGE_DEFAULT,
        };
    }
    return properties;
}

pub fn register_signal(
    class_type: type,
    _signal_name: [:0]const u8,
    template_func: anytype,
    arg_names: ?[]const ?[:0]const u8,
) void {
    var signal_name = Zig2Godot.stringName(_signal_name);
    defer signal_name.deinit();
    var properties = function_to_propinfo(false, template_func, arg_names);
    defer function_to_propinfo_deinit(&properties);
    API.fns.classdb_register_extension_class_signal.?(
        API.class_library,
        &find_class(class_type).CLASSNAME,
        &signal_name,
        &properties,
        properties.len,
    );
}

pub fn register_single_enum(
    class_type: type,
    enum_name: [:0]const u8,
    value_name: [:0]const u8,
    value: i64,
    bitfield: bool,
) void {
    const enumname = Zig2Godot.stringName(enum_name);
    defer enumname.deinit();
    const propname = Zig2Godot.stringName(value_name);
    defer propname.deinit();
    API.fns.classdb_register_extension_class_integer_constant.?(
        API.class_library,
        &find_class(class_type).CLASSNAME,
        &enumname,
        &propname,
        value,
        if (bitfield) 1 else 0,
    );
}

pub fn register_enums(class_type: type, enum_type: type, is_bitfield: bool) void {
    switch (@typeInfo(enum_type)) {
        .Enum => |e| {
            var enumname = Zig2Godot.stringName(get_class_name(enum_type));
            defer enumname.deinit();
            for (e.fields) |field| {
                var prop_name = Zig2Godot.stringName(field.name);
                defer prop_name.deinit();
                API.fns.classdb_register_extension_class_integer_constant.?(
                    API.class_library,
                    &find_class(class_type).CLASSNAME,
                    &enumname,
                    &prop_name,
                    @field(enum_type, field.name),
                    is_bitfield,
                );
            }
        },
        else => @compileError("type is not of Enum"),
    }
}

pub const MethodOpts = struct {
    arg_names: ?[]const ?[:0]const u8 = null,
    ret_hint: ?[:0]const u8 = null,
    ret_class_name: ?[:0]const u8 = null,
    flags: u32 = 0,
};

fn mem_eql(a: anytype, b: @TypeOf(a)) bool {
    return std.mem.eql(u8, &std.mem.toBytes(a), &std.mem.toBytes(b));
}

/// `class_type` - the owning registered class
/// `method_name` - the name as it will appear in godot
/// `func` - the function you are registering
pub fn register_method(
    class_type: type,
    methodname: [:0]const u8,
    func: anytype,
    opts: MethodOpts,
    // arg_names: ?[]const ?[:0]const u8,
    // TODO UserData
) void {
    var classname = find_class(class_type).CLASSNAME;
    const fninfo: std.builtin.Type.Fn = @typeInfo(@TypeOf(func)).Fn;

    var method_name = Zig2Godot.stringName(methodname);
    defer method_name.deinit();

    var METHODINFO = interface.ClassMethodInfo{
        .name = @constCast(@ptrCast(&method_name)),
        .method_userdata = null,
        .call_func = undefined,
        .ptrcall_func = undefined,
        .method_flags = undefined,
        .has_return_value = undefined,
        .return_value_info = undefined,
        .return_value_metadata = undefined,
        .argument_count = undefined,
        .arguments_info = undefined,
        .arguments_metadata = undefined,
        .default_argument_count = undefined,
        .default_arguments = undefined,
    };

    // labels fuck up the stack trace and I need this varible to be const
    const is_static = comptime __is_static(fninfo, class_type);
    std.log.debug("\nRegistering '{s}' '{any}'", .{ methodname, @TypeOf(func) });
    std.log.debug("{s}", .{if (is_static) "Static(First param isn't of *" ++ @typeName(class_type) ++ ")" else "Instance"});
    METHODINFO.method_flags = if (is_static) enums.MethodFlags.METHOD_FLAG_STATIC else enums.MethodFlags.METHOD_FLAGS_DEFAULT;
    METHODINFO.method_flags |= opts.flags;
    const has_return = if (fninfo.return_type) |T| T != void else @compileError("Return type of function is null.");
    std.log.debug("Returns {s}", .{@typeName(fninfo.return_type.?)});
    if (has_return) {
        METHODINFO.has_return_value = 1;
        var empty = Zig2Godot.stringName("");
        defer empty.deinit();
        var ret_hint = if (opts.ret_hint) |h| Zig2Godot.stringName(h) else empty;
        defer if (mem_eql(ret_hint, empty)) ret_hint.deinit();
        var ret_class_name = if (opts.ret_class_name) |h| Zig2Godot.stringName(h) else empty;
        defer if (mem_eql(ret_class_name, empty)) ret_class_name.deinit();
        METHODINFO.return_value_info = @constCast(&interface.PropertyInfo{
            .type = @intFromEnum(Zig2Godot.typing_enum(fninfo.return_type.?) catch @panic("cannot ignore error")),
            .name = @ptrCast(&empty),
            .class_name = @ptrCast(&ret_class_name),
            .hint = @intFromEnum(enums.PropertyHint.PROPERTY_HINT_NONE),
            .hint_string = @ptrCast(&ret_hint),
            .usage = enums.PropertyUsageFlags.PROPERTY_USAGE_NONE,
        });
        METHODINFO.return_value_metadata = to_methodarg_metadata(fninfo.return_type.?);
        std.log.debug("Return type converted to {s}", .{@tagName(Zig2Godot.typing_enum(fninfo.return_type.?) catch unreachable)});
        std.log.debug("Return type metadata {d}", .{to_methodarg_metadata(fninfo.return_type.?)});
    } else {
        if (opts.ret_hint) |_| {
            std.log.err("Return type hint provided, but function has no return type", .{});
        }
        if (opts.ret_class_name) |_| {
            std.log.err("Return type class name provided, but function has no return type", .{});
        }
        METHODINFO.has_return_value = 0;
        METHODINFO.return_value_info = null;
        METHODINFO.return_value_metadata = interface.MethodArgumentMetadata.NONE;
    }
    const param_offset = if (is_static) 0 else 1;
    const param_count = fninfo.params.len - param_offset;
    METHODINFO.argument_count = param_count;
    // having tihs as [..]PropInfo crash the compiler :(
    var param_metadata: [param_count]interface.ClassMethodArgumentMetadata = undefined;
    if (opts.arg_names != null and opts.arg_names.?.len > param_count) std.log.err("More argument names provided than arguments for function {s}. Remember self isn't included", .{methodname});

    var properties = function_to_propinfo(is_static, func, opts.arg_names);
    defer function_to_propinfo_deinit(&properties);
    inline for (fninfo.params[param_offset..], &param_metadata) |param, *metadata| {
        metadata.* = to_methodarg_metadata(param.type.?);
    }
    METHODINFO.arguments_info = @ptrCast(&properties);
    METHODINFO.arguments_metadata = @ptrCast(&param_metadata);

    METHODINFO.default_argument_count = 0; // TODO
    METHODINFO.default_arguments = null; // TODO
    METHODINFO.call_func = &struct {
        fn call_func(
            metadata: ?*anyopaque,
            _self: interface.ClassInstancePtr,
            args: [*c]const interface.ConstVariantPtr,
            argc: i64,
            ret: interface.VariantPtr,
            errors: [*c]interface.CallError,
        ) callconv(.C) void {
            // std.log.debug("call_fn", .{});
            _ = argc; // autofix
            _ = metadata; // autofix
            _ = errors; // autofix
            var rv: if (has_return) fninfo.return_type.? else void = undefined;
            // var rv: ?fninfo.return_type.? = null;
            if (is_static and param_count == 0) {
                rv = @call(.always_inline, func, .{});
            } else {
                var fn_args: std.meta.ArgsTuple(@TypeOf(func)) = undefined;
                if (!is_static) fn_args[0] = @ptrCast(@alignCast(_self));
                inline for (0..param_count) |i| {
                    fn_args[i + param_offset] = Godot2Zig.variant_opaque(@TypeOf(fn_args[i + param_offset]), args[i]);
                }

                rv = @call(.always_inline, func, fn_args);
            }
            if (has_return) {
                if (ret) |r|
                    // @as(*@TypeOf(rv), @ptrCast(@alignCast(r))).* = rv
                    @as(*types.Variant, @ptrCast(@alignCast(r))).* = Zig2Godot.variant(rv)
                else
                    @panic("ret address is null.");
            }
        }
    }.call_func; // TODO
    METHODINFO.ptrcall_func = &struct {
        fn ptrcall_func(
            metadata: ?*anyopaque,
            _self: interface.ClassInstancePtr,
            args: [*c]const interface.ConstTypePtr,
            ret: interface.TypePtr,
        ) callconv(.C) void {
            // std.log.debug("ptrcall_fn is currently broken! It treats TypedPtrs as if they were Variants. They are not.", .{});
            _ = metadata; // autofix
            var rv: if (has_return) fninfo.return_type.? else void = undefined;
            if (is_static and param_count == 0) {
                rv = @call(.always_inline, func, .{});
            } else {
                var fn_args: std.meta.ArgsTuple(@TypeOf(func)) = undefined;
                if (!is_static) fn_args[0] = @ptrCast(@alignCast(_self));

                inline for (0..param_count) |i| {
                    const ParamType = @TypeOf(fn_args[i + param_offset]);
                    fn_args[i + param_offset] = Godot2Zig.typed_value(ParamType, args[i]);
                }

                rv = @call(.always_inline, func, fn_args);
            }
            if (has_return) {
                if (ret) |r| { // FIXME: While this works, it'd be cheaper if I could avoid creating a variant
                    const typing = comptime Zig2Godot.typing_enum(@TypeOf(rv)) catch @panic("Failed to convert to type");
                    const ReturnT: type = convert.typeenum_to_type(typing);
                    @as(*ReturnT, @ptrCast(@alignCast(r))).* = Zig2Godot.typed_value(ReturnT, rv);
                } else @panic("ret address is null.");
            }
        }
    }.ptrcall_func;
    API.fns.classdb_register_extension_class_method.?(
        API.class_library,
        @ptrCast(&classname),
        @ptrCast(&METHODINFO),
    );
}
