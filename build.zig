const std = @import("std");

const STAGE1_FILES: []const []const u8 = &.{
    "gdextension_interface.h",
    "extension_api.json",
    "gdextension/gdextension_interface.zig",
};
const CODEGEN_FILE = "src/codegen.zig";
const STAGE2_FILES: []const []const u8 = &.{
    "gdextension/gdextension_classes_api.zig",
    "gdextension/gdextension_classes_impl.zig",
    "gdextension/gdextension_type_api.zig",
    "gdextension/gdextension_type_impl.zig",
    "gdextension/gdextension_utility_functions.zig",
    "gdextension/gdextension_utility_fn_impl.zig",
    "gdextension/gdextension_global_enums.zig",
    "gdextension/gdextension_global_constants.zig",
    "gdextension/gdextension_singletons.zig",
    "gdextension/gdextension_native_structures.zig",
    "gdextension/gdextension_interface_fns.zig",
};

fn assert_exist(files: []const []const u8) bool {
    for (files) |name| {
        var file = std.fs.cwd().openFile(name, .{}) catch return false;
        defer file.close();
    }
    return true;
}
fn assert_oldest(oldest: []const u8, others: []const []const u8) !bool {
    const old = try std.fs.cwd().statFile(oldest);
    for (others) |other| {
        const o = try std.fs.cwd().statFile(other);
        if (old.mtime > o.mtime) return false;
    }
    return true;
}

fn is_enum(id: []const u8) bool {
    // if (id[id.len - 1] == ' ') @panic("");
    // VARIANT_TYPE_NIL
    if (id[0] == '_') return false;
    if (std.mem.eql(u8, "INTERFACE_H", id)) return false;
    if (std.mem.eql(u8, "NULL", id)) return false;
    if (std.mem.startsWith(u8, id, "UINT")) return false;
    if (std.mem.startsWith(u8, id, "INT")) return false;
    if (std.mem.startsWith(u8, id, "PTRDIFF_")) return false;
    if (std.mem.startsWith(u8, id, "SIG_ATOMIC_")) return false;
    if (std.mem.startsWith(u8, id, "SIZE_")) return false;
    if (std.mem.startsWith(u8, id, "WCHAR_")) return false;
    if (std.mem.startsWith(u8, id, "WINT_")) return false;
    for (id) |char| {
        if (char == '_')
            continue
        else if (char >= 'A' and char <= 'Z')
            continue
        else if (char >= '0' and char <= '9')
            continue
        else
            return false;
    }
    return true;
}

fn clean_prefix(prefix: []u8) []const u8 {
    if (std.mem.eql(u8, prefix, "VARIANT_TYPE_")) {
        return "VariantTypes";
    }
    if (std.mem.eql(u8, prefix, "INITIALIZATION_")) {
        return "InitLevels";
    }
    var out = prefix;
    var cap = true;
    if (out[out.len - 1] == '_') out = out[0 .. out.len - 1];
    // std.debug.print("{s}\n", .{out});
    var index: usize = 0;
    var char: usize = 0;
    while (index < out.len) : ({
        index += 1;
        char += 1;
    }) {
        while (out[index] == '_') {
            index += 1;
            cap = true;
        }
        if (cap) {
            cap = false;
            out[char] = std.ascii.toUpper(out[index]);
        } else {
            out[char] = std.ascii.toLower(out[index]);
        }
    }
    return out[0..char];
}

fn transformer(alloc: std.mem.Allocator, _source: []u8) ![]const u8 {
    var buffer = std.ArrayList(u8).init(alloc);
    var header = _source;
    // Remove Prefix1
    const key0 = "GDExtension";
    const transformed0 = std.mem.replace(u8, header, key0, "", header);
    header = header[0 .. header.len - key0.len * transformed0];

    // Remove Prefix2
    const key1 = "GDEXTENSION_";
    const transformed1 = std.mem.replace(u8, header, key1, "", header);
    header = header[0 .. header.len - key1.len * transformed1];
    // if (true) {
    //     return alloc.dupe(u8, header);
    // }
    var enum_buffer = std.ArrayList(u8).init(alloc);
    var step: enum { searching, comparing, filling } = .searching;
    var previous: ?[]u8 = null;
    var prefix: ?[]const u8 = null;
    var prefix_len: usize = 0;
    var iter = std.mem.splitScalar(u8, header, '\n');
    while (iter.next()) |line| {
        if (std.mem.startsWith(u8, line, "pub const ")) {
            const _identifier = line["pub const ".len..];
            const identifier = _identifier[0..std.mem.indexOfAny(u8, _identifier, ": ").?];
            // std.debug.print("-{s}\n", .{identifier});
            // and (prefix == null or
            // (prefix != null and std.mem.startsWith(u8, identifier, prefix.?)))
            if (is_enum(identifier)) {
                // std.debug.print("{s} {s}\n", .{ @tagName(step), identifier });
                switch (step) {
                    .searching => {
                        previous = @constCast(_identifier);
                        step = .comparing;
                    },
                    .comparing => {
                        for (0..@min(previous.?.len, identifier.len)) |char| {
                            if (previous.?[char] != identifier[char]) {
                                prefix_len = char;
                                prefix = identifier[0..char];
                                try enum_buffer.appendSlice("// Transformed to enum\npub const ");
                                try enum_buffer.appendSlice(clean_prefix(previous.?[0..char]));
                                try enum_buffer.appendSlice(" = struct {\n");
                                try enum_buffer.appendSlice("pub const ");
                                try enum_buffer.appendSlice(previous.?[prefix_len..]);
                                try enum_buffer.append('\n');
                                try enum_buffer.appendSlice("pub const ");
                                try enum_buffer.appendSlice(_identifier[prefix_len..]);
                                try enum_buffer.append('\n');
                                step = .filling;
                                break;
                            }
                        }
                    },
                    .filling => {
                        try enum_buffer.appendSlice("pub const ");
                        try enum_buffer.appendSlice(_identifier[prefix_len..]);
                        try enum_buffer.append('\n');
                    },
                }
                continue;
            }
        }
        switch (step) {
            .searching => {},
            .comparing => @panic("This should have been handled elsewhere"),
            .filling => {
                try enum_buffer.appendSlice("\n};\n");
                try buffer.appendSlice(enum_buffer.items);
                enum_buffer.clearRetainingCapacity();
                step = .searching;
                previous = null;
                prefix_len = 0;
                prefix = null;
            },
        }
        try buffer.appendSlice(line);
        try buffer.append('\n');
    }
    return buffer.items;
}

// Cannot capture pointer for makeFn
pub var translate: *std.Build.Step.TranslateC = undefined;
fn gen_files(b: *std.Build, target: std.Build.ResolvedTarget, opt: std.builtin.OptimizeMode) !?*std.Build.Step {
    var leading_step: ?*std.Build.Step = null;
    if (!assert_exist(STAGE1_FILES)) {
        const get_api = b.addSystemCommand(&.{
            "godot",
            "--headless",
            "--dump-extension-api",
            "--dump-gdextension-interface",
        });
        b.default_step.dependOn(&get_api.step);

        translate = b.addTranslateC(.{
            .root_source_file = b.path("gdextension_interface.h"),
            .target = target,
            .optimize = opt,
        });
        translate.step.dependOn(&get_api.step);
        b.default_step.dependOn(&translate.step);
        const transform = b.step("Transform translate c", "Transform");
        transform.makeFn = &struct {
            pub fn make(step: *std.Build.Step, prog_node: std.Progress.Node) anyerror!void {
                const node = prog_node.start("Transforming", 3);
                var file = try std.fs.openFileAbsolute(translate.getOutput().generated.file.getPath(), .{});
                defer file.close();
                const content = try file.readToEndAlloc(step.owner.allocator, 10 * 1024 * 1024);
                defer step.owner.allocator.free(content);
                const transformed = try transformer(step.owner.allocator, content);
                defer step.owner.allocator.free(transformed);

                var out = try std.fs.cwd().createFile("gdextension/gdextension_interface.zig", .{});
                defer out.close();
                try out.writeAll(transformed);
                node.completeOne();
            }
        }.make;
        transform.dependOn(&translate.step);
        b.default_step.dependOn(transform);
        leading_step = transform;
    }

    if (!assert_exist(STAGE2_FILES) or !try assert_oldest(CODEGEN_FILE, STAGE2_FILES)) {
        var step = b.step("godot codegen", "Generate zig source files based on extension_api.json");
        step.makeFn = &struct {
            pub fn f(_step: *std.Build.Step, prog_node: std.Progress.Node) anyerror!void {
                _ = _step;
                try @import("src/codegen.zig").gencode("./extension_api.json", "./gdextension_interface.h", prog_node);
            }
        }.f;
        if (leading_step) |l_step| {
            step.dependOn(l_step);
        }
        b.default_step.dependOn(step);
        leading_step = step;
    }
    return leading_step;
}

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const opt = b.standardOptimizeOption(.{});
    // const name = "godotExtension";
    _ = try gen_files(b, target, opt);
    const gdapi = b.addModule("gdapi", .{
        .root_source_file = b.path("api.zig"),
        .target = target,
        .optimize = opt,
    });

    _ = gdapi; // autofix
}
