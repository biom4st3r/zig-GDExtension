const API = @import("api.zig");
const interface = API.interface;
const std = @import("std");
const enums = API.enums;
const types = API.types;
var fns = &API.fns;

pub fn to_ClassCallVirtual(self_type: type, f: anytype) interface.ClassCallVirtual {
    comptime var T: type = @TypeOf(f);
    inline while (true) {
        switch (@typeInfo(T)) {
            .Pointer => |p| T = p.child,
            .Fn => break,
            else => @compileError("to_ClassCallVirtual only accept Functions. Not " ++ @typeName(T)),
        }
    }
    return @ptrCast(&struct {
        fn func(_self: *self_type, args: [*c]interface.ConstTypePtr, ret: interface.TypePtr) callconv(.C) void {
            const info = @typeInfo(T).Fn;

            var fn_args: std.meta.ArgsTuple(T) = undefined;
            fn_args[0] = @ptrCast(@alignCast(_self));
            inline for (0..info.params.len - 1) |i| {
                fn_args[i + 1] = Godot2Zig.variant_opaque(@TypeOf(fn_args[i + 1]), args[i]);
            }
            const rv = @call(.always_inline, f, fn_args);
            if (info.return_type != void) {
                if (ret) |r|
                    @as(*types.Variant, @ptrCast(@alignCast(r))).* = Zig2Godot.variant(rv)
                else
                    @panic("ret address is null.");
            }
        }
    }.func);
}

fn int_cast(T: type, val: i64) T {
    const info: std.builtin.Type = @typeInfo(T);
    if (info.Int.signedness == .signed) {
        return @intCast(val);
    }
    const usign: u64 = @bitCast(val);
    return @intCast(usign);
}
pub const Godot2Zig = struct {
    pub fn variant_opaque(T: type, vari: API.interface.ConstVariantPtr) T {
        if (vari) |v| {
            return variant(T, @ptrCast(@alignCast(@constCast(v))));
        } else @panic("Godot2Zig.variant_opaque received null Variant");
    }
    pub fn typed_value(T: type, val: API.interface.ConstTypePtr) T {
        if (val) |v| {
            const info = @typeInfo(T);
            switch (info) {
                .Int => {
                    return int_cast(T, @as(*const i64, @ptrCast(@alignCast(v))).*);
                },
                .Float => {
                    return @floatCast(@as(*const T, @ptrCast(@alignCast(v))).*);
                },
                .Bool => {
                    return @as(*const T, @ptrCast(@alignCast(v))).*;
                },
                .Struct => {
                    return @as(*const T, @ptrCast(@alignCast(v))).*;
                },
                else => {
                    @panic("Type not handled " ++ @typeName(T));
                },
            }
        } else {
            @panic("Godot2Zig.typed received null Variant");
        }
    }
    pub fn variant(T: type, vari: *types.Variant) T {
        if (T == *types.Variant) return @ptrCast(vari);
        API.util_fns.general.print(&.{vari});

        return switch (@typeInfo(T)) {
            .Int => int_cast(T, vari.to_Int()), // Need to handle truncating bits. Godot returns only i64 and we need to bitwise i64 -> u8 or whatever
            .Float => vari.to_Float(),
            .Bool => vari.to_Bool(),
            // .Struct => @compileError("TODO Implement. from_vari Struct " ++ @typeName(T)), // TODO
            .Pointer => {
                switch (T) {
                    else => @compileError("Godot2Zig.variant cannot return pointers " ++ @typeName(T)),
                }
            },
            else => {
                const fn_name = comptime "to_" ++ after_x(@typeName(T), '.').?;
                std.debug.print("from_vari: {s}\n", .{fn_name});
                if (std.meta.hasMethod(types.Variant, fn_name)) {
                    return @call(.auto, @field(types.Variant, fn_name), .{vari});
                }
                @compileError("Godot2Zig.variant cannot convert type properly " ++ @typeName(T));
            },
        }; // autofix
    }
    /// Straight from godot-zig/godot-zig. Thank you so much.
    pub fn string_buff(T: type, str: types.String, buffer: []T) []T {
        const fnt = switch (T) {
            u8 => API.fns.string_to_utf8_chars.?,
            u16 => API.fns.string_to_utf16_chars.?,
            u32 => API.fns.string_to_utf32_chars.?,
            else => @compileError("string_buff: only u8, u16, u32 are supported. Not " ++ @typeName(T)),
        };
        const size = @call(.auto, fnt, .{ &str, buffer.ptr, @as(i64, @intCast(buffer.len)) });
        // const size = API.fns.string_to_latin1_chars.?(&str, buffer.ptr, @intCast(buffer.len));
        return buffer[0..@intCast(size)];
    }
    // to_zigstring0
    pub fn stringName(strn: *const types.StringName, buffer: []u8) []u8 {
        const str = types.String.init2(strn.*);
        return string_buff(str, buffer);
    }
};
fn cast(Dest: type, val: anytype) Dest {
    return @as(*const Dest, @ptrCast(@alignCast(&val))).*;
}
pub const Zig2Godot = struct {
    pub fn typed_value(ReturnT: type, rv: anytype) ReturnT {
        const info = @typeInfo(@TypeOf(rv));
        switch (info) {
            .Int => |i| {
                if (i.bits > 64) @panic("Cannot pass integers larger than 64 to godot.");
                return cast(types.int, @as(i64, @intCast(rv)));
            },
            .Float => {
                const FloatType = if (API.constants.BUILD_CONFIG == .float_64 or API.constants.BUILD_CONFIG == .double_64) f64 else f32;
                return cast(types.float, @as(FloatType, @floatCast(rv)));
            },
            .Bool => {
                return cast(types.bool, rv);
            },
            else => {
                @compileError("Type not yet handled by Zig2Godot.typed_value " + @typeName(@TypeOf(rv)));
            },
        }
    }
    pub fn variant(value: anytype) types.Variant {
        const TI = @typeInfo(@TypeOf(value));
        if (std.meta.hasMethod(@TypeOf(value), "to_variant")) {
            return value.to_variant().?;
        }
        switch (TI) {
            .Int => |_| return types.int.to_variant(&@intCast(value)).?,
            .Float => |_| return types.float.to_variant(@floatCast(&value)).?,
            .Bool => return types.bool.to_variant(@intFromBool(&value)).?,
            else => {},
        }
        @panic("Unhandled to_variant type " ++ @typeName(@TypeOf(value))).?;
    }
    pub fn string(buff: [:0]const u8) types.String {
        var str: types.String = undefined;
        fns.string_new_with_utf8_chars.?(&str, @ptrCast(buff.ptr));
        return str;
    }

    pub fn stringName(buff: [:0]const u8) types.StringName {
        var str: types.String = string(buff);
        defer str.deinit();
        return types.StringName.init2(str);
    }
    pub fn typing_enum(T: type) !types.Variant.Type {
        // https://github.com/godotengine/godot/blob/1b2e0b32d727ceab387afcabba422f994038e439/core/variant/variant_utility.cpp#L1388
        if (T == *types.Variant) return types.Variant.Type.Nil;
        const info = @typeInfo(T);
        return switch (info) {
            .Void => types.Variant.Type.Nil,
            .Int => types.Variant.Type.Int,
            .Enum => types.Variant.Type.Int,
            .Float => types.Variant.Type.Float,
            .Bool => types.Variant.Type.Bool,
            .Optional => |o| {
                return typing_enum(o.child);
            },
            .Pointer => |p| typing_enum(p.child),
            .Struct => {
                inline for (@typeInfo(types).Struct.decls) |d| {
                    if (std.mem.endsWith(u8, @typeName(T), d.name)) {
                        return std.meta.stringToEnum(types.Variant.Type, d.name).?;
                    }
                }
                std.debug.print("unknown struct to_godot_type {s}.", .{@typeName(T)});
                unreachable;
            },
            else => {
                std.debug.print("unknown type to_godot_type {s}.", .{@typeName(T)});
                return error.UnknownType;
            },
        };
    }
};

pub fn typeenum_to_type(T: types.Variant.Type) type {
    inline for (@typeInfo(types).Struct.decls) |decl| {
        const DECL = @field(types, decl.name);
        if (@hasDecl(DECL, "GD_TYPE") and @field(DECL, "GD_TYPE") == T) return DECL;
    }
    unreachable;
}

fn after_x(string: [:0]const u8, c: u8) if (@inComptime()) ?[:0]const u8 else ?[]const u8 {
    const i = std.mem.lastIndexOf(u8, string, &[1]u8{c});
    if (i) |ii| return string[ii + 1 ..];
    return null;
}

// pub fn from_godot(T: type, typedPtr: ?*anyopaque) T {
//     const info: std.builtin.Type = @typeInfo(T);
//     switch (info) {
//         .Int => {
//             const typed: *API.types.int = @ptrCast(@alignCast(typedPtr.?));
//             return int_cast(T, typed.*);
//         },
//         .Float => {
//             const typed: *if (API.constants.BUILD_CONFIG == .float_64 or API.constants.BUILD_CONFIG == .double_64) f64 else f32 = @ptrCast(@alignCast(typedPtr.?));
//             return @floatCast(typed.*);
//         },
//     }
// }
