pub const classes_api = @import("gdextension/gdextension_classes_api.zig");
pub const classes = @import("gdextension/gdextension_classes_impl.zig");
pub const _type_api = @import("gdextension/gdextension_type_api.zig");
pub const types = @import("gdextension/gdextension_type_impl.zig");
pub const enums = @import("gdextension/gdextension_global_enums.zig");
pub const constants = @import("gdextension/gdextension_global_constants.zig");
pub const util_fns_api = @import("gdextension/gdextension_utility_functions.zig");
pub const util_fns = @import("gdextension/gdextension_utility_fn_impl.zig");
pub const InterfaceFunctions = @import("gdextension/gdextension_interface_fns.zig");
pub var fns = InterfaceFunctions{};
pub const singletons = @import("gdextension/gdextension_singletons.zig");
pub const interface = @import("gdextension/gdextension_interface.zig");

const std = @import("std");
pub const convert = @import("conversion.zig");
pub const register = @import("registration.zig");

pub var class_library: interface.ClassLibraryPtr = null;

var initilized = false;
test {
    classes_api.Node.init();
    fns.mem_alloc;
    // classes.Node.set_name(, )
}
pub fn init(_p_get_proc_address: interface.InterfaceGetProcAddress, class_lib: interface.ClassLibraryPtr) !void {
    defer initilized = true;
    if (initilized) {
        std.log.err("Already initlized!", .{});
        return;
    }
    class_library = class_lib;
    // TODO:
    // * make all vararg util fns usable
    // * A lot of `types` instance functions require a @constCast to properly call.
    // * Some functions that wrap args in [_]*const anyopaque{} have 1 as the arg length, but should be 0
    // String.to_ascii_buffer(and friends) segfault. I assume null pointer but I don't know where from?
    init_interface_functions(&fns, _p_get_proc_address);
    // Early init
    // These are needed to initilize the rest of the functions
    _type_api.StringNameApi.init2 = @ptrCast(fns.variant_get_ptr_constructor.?(interface.VariantTypes.STRING_NAME, 2));
    _type_api.StringApi.deinit = @ptrCast(fns.variant_get_ptr_destructor.?(interface.VariantTypes.STRING));
    std.log.info("Init util func", .{});
    init_util_func(fns);
    std.log.info("Init type api", .{});
    init_type_api(fns);
    std.log.info("Init singletons", .{});
    init_singletons(fns);
    std.log.info("Saying Hello from Godot", .{});
    util_fns.general.print(&.{&convert.Zig2Godot.string("Hello from Zig!").to_variant().?});
}

pub fn init_interface_functions(self: *InterfaceFunctions, p_get_proc_address: interface.InterfaceGetProcAddress) void {
    const t: std.builtin.Type = @typeInfo(InterfaceFunctions);
    inline for (t.Struct.fields) |field| {
        @field(self, field.name) = @as(field.type, @ptrCast(p_get_proc_address.?(field.name)));
    }
}

pub fn init_singletons(i: InterfaceFunctions) void {
    inline for (@typeInfo(singletons).Struct.decls) |decl| {
        var string_name = convert.Zig2Godot.stringName(decl.name);
        defer string_name.deinit();
        if (i.global_get_singleton.?(&string_name)) |func| {
            @field(singletons, decl.name) = @ptrCast(@alignCast(func));
        } else {
            std.log.err("Failed to load singleton {s}", .{decl.name});
        }
    }
}

pub fn init_util_func(i: InterfaceFunctions) void {
    inline for (@typeInfo(util_fns_api).Struct.decls) |_struct| {
        const sub_struct = @field(util_fns_api, _struct.name);
        inline for (@typeInfo(sub_struct).Struct.decls) |func| {
            @setEvalBranchQuota(200_000);
            const split = comptime std.mem.indexOf(u8, func.name, " ").?;

            const functionname: [:0]const u8 = @ptrCast(func.name[0..split] ++ [_]u8{0}); //@ptrCast(alloc.alloc(u8, split) catch unreachable);
            var ctor_string_name = convert.Zig2Godot.stringName(functionname);
            ctor_string_name.deinit();
            const ufunc = i.variant_get_ptr_utility_function.?(
                @ptrCast(&ctor_string_name),
                @intCast(std.fmt.parseInt(u64, func.name[split + 1 ..], 0) catch unreachable),
            );
            if (ufunc == null) std.log.err("Failed to fetch util func {s}", .{functionname}) else @field(sub_struct, func.name) = @ptrCast(ufunc);
        }
    }
}
fn find_variant_type(comptime _name: []const u8) ?interface.VariantType {
    const name = comptime _name[0 .. _name.len - 3];
    comptime var v: ?interface.VariantType = 0;
    comptime label: {
        if (std.mem.eql(u8, _name, "VariantApi")) {
            v = null;
            break :label;
        }
        for (@typeInfo(interface.VariantTypes).Struct.decls) |decl| {
            var type_name: [decl.name.len]u8 = undefined;
            var input: u64 = 0;
            var output: u64 = 0;
            while (input < type_name.len) : ({
                input += 1;
                output += 1;
            }) {
                while (decl.name[input] == '_') input += 1;
                type_name[output] = decl.name[input];
            }
            // @compileLog(name, type_name);
            if (std.ascii.eqlIgnoreCase(type_name[0..output], name)) {
                v = @field(interface.VariantTypes, decl.name);
                break :label;
            }
        }
        @compileError("NameNotFound" ++ _name);
    }
    return v;
}
pub fn init_type_api(i: InterfaceFunctions) void {
    const type_api: std.builtin.Type = @typeInfo(_type_api);
    inline for (type_api.Struct.decls) |struct_field| {
        const class = @field(_type_api, struct_field.name);
        const class_type = @typeInfo(class);
        switch (class_type) {
            .Struct => {},
            else => continue,
        }
        // Hacky just to handle VariantApi
        const vtype: ?interface.VariantType = comptime find_variant_type(struct_field.name);
        // const vtype = @intFromEnum(@field(types.Variant.Type, struct_field.name[0 .. struct_field.name.len - 3]));
        inline for (class_type.Struct.decls) |class_field| {
            @setEvalBranchQuota(200_000);
            if (comptime std.mem.indexOf(u8, class_field.name, " ")) |idx| {
                const class_field_name: [:0]const u8 = @ptrCast(class_field.name[0..idx] ++ [_]u8{0});
                var fn_name: types.StringName = convert.Zig2Godot.stringName(class_field_name);
                defer fn_name.deinit();
                const hash = comptime std.fmt.parseInt(i64, class_field.name[idx + 1 ..], 0) catch unreachable;
                if (i.variant_get_ptr_builtin_method.?(
                    vtype.?,
                    &fn_name,
                    hash,
                )) |f| {
                    @field(class, class_field.name) = @ptrCast(f);
                } else {
                    std.log.err("Failed to fetch class function {s}.{s}", .{ struct_field.name, class_field.name });
                }
            } else if (comptime std.mem.eql(u8, "init", class_field.name[0..4])) {
                const hash = comptime std.fmt.parseInt(u8, class_field.name[4..], 0) catch unreachable;
                if (i.variant_get_ptr_constructor.?(
                    vtype.?,
                    hash,
                )) |f| {
                    @field(class, class_field.name) = @ptrCast(f);
                } else {
                    std.log.err("Failed to fetch class init {s}.{s}", .{ struct_field.name, class_field.name });
                }
            } else if (comptime std.mem.eql(u8, "deinit", class_field.name[0..6])) {
                if (i.variant_get_ptr_destructor.?(
                    vtype.?,
                )) |f| {
                    @field(class, class_field.name) = @ptrCast(f);
                } else {
                    std.log.err("Failed to fetch class deinit {s}.{s}", .{ struct_field.name, class_field.name });
                }
            } else if (comptime std.mem.eql(u8, "to_variant", class_field.name)) {
                if (i.get_variant_from_type_constructor.?(vtype.?)) |ctor| {
                    @field(class, class_field.name) = ctor;
                } else {
                    std.log.err("Failed to fetch Type -> Variant init {s}.{s}", .{ struct_field.name, class_field.name });
                }
            } else if (comptime std.mem.eql(u8, "VariantApi", struct_field.name)) {
                // std.debug.print("{s}\n", .{class_field.name});
                if (i.get_variant_to_type_constructor.?(@intFromEnum(std.meta.stringToEnum(types.Variant.Type, class_field.name[3..]).?))) |f| {
                    @field(class, class_field.name) = f;
                } else {
                    std.log.err("Failed to fetch Variant -> type ctor {s}", .{class_field.name});
                }
            }
        }
    }
}
