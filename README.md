# Godot Binding for Zig!


## How to use this library
1. Ensure godot is on your path

Linux
```
PATH="/path/to/godot:$PATH"
```

Windows
```
(Win+Pause) > Advanced System Settings > Advanced > Environemnt Variables > System Variables > Edit PATH
```
2. Add this repo to your build files
```
### .zon ###
.gdext = .{
  .url = "https://gitlab.com/biom4st3r/zig-GDExtension/-/archive/default/zig-GDExtension-default.tar.gz"
}
### .zig ###
const gdext = b.dependency("gdext", .{});
exe.addModule("gdext", gdext.module("gdext"))
// You'll also want to compile to a shared library

const library = b.addSharedLibrary(.{
    .name = "my-gdextension",
    .root_source_file = .{ .path = "src/main.zig" },
    .target = b.host,

    .optimize = b.standardOptimizeOption(.{}),
    // .version = .{ .major = 0, .minor = 0, .patch = 1 },
});
```
3. Generate bindings for your version of Godot
```
TODO
```
4. Define an entrypoint
```
### Main.zig ###
const GD = @import("gdext");
pub fn initalize(userdata: *anyopaque, p_level: gde.interface.InitializationLevel) callconv(.C) void {
    if (p_level == gde.interface.InitLevel.EDITOR) {
        gde.init(get_proc, class_library) catch unreachable; // autofix
    }
    _ = userdata; // autofix
}
pub fn deinitalize(userdata: *anyopaque, p_level: gde.interface.InitializationLevel) callconv(.C) void {
    _ = userdata; // autofix
}

export fn entrypoint(
    p_get_proc_address: gde.interface.InterfaceGetProcAddress,
    p_library: gde.interface.ClassLibraryPtr,
    r_init: *gde.interface.Initialization,
) bool {
    get_proc = p_get_proc_address;
    class_library = p_library;
    r_init.initialize = initalize;
    r_init.deinitialize = deinitalize;
    return true;
}
```
5.  Compile your extension
```
zig build
```
6. Copy the library from zig-out/libs/my-extension.so and create a x.gdextension in your godot project directory
```
[configuration]
entry_symbol = "entrypoint"
compatibility_minimum = "4.1"

[libraries]
linux.x86_64 = "my-gdextension.so"
```

## Generating bindings
* TODO
## Layout
### gdex
* init - First be the first think called in your Extention. It fetch ClassInstanceFunctions, UtilityFunction, and other backend stuff for interoping with Godot.
* classes - Builtin classes like Node, Node3D, AudioStream...
* types - Builtin types like Variant, Vector3, Quaternion, PackedByteArray...
* enums - Enums and bitfields like Orientation, Key, PropertyHint...
* util_fns - Utility Functions like print, math function, random
* fns - Interface Function for interacting with Godot.

### convert
* Some utility functinos for converting to and from godot types.
### reg
* Wrappers for fns.classdb_register_x.
